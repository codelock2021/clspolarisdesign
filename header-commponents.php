<?php include_once('header.php'); ?>
<?php include_once('sidebar.php'); ?>
<div class="Polaris-Page Polaris-Page--fullWidth">
    <!-- our app dropdown block start-->
    <div class="app-dropdown-menu Polaris-Page__Header Polaris-Page__Header--hasSecondaryActions">
        <div class="dropdown">
            <button class="Polaris-Button Polaris-Button--primary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 55 55" focusable="false" aria-hidden="true"><path d="M21.66,24H2.34C1.048,24,0,22.952,0,21.66V2.34C0,1.048,1.048,0,2.34,0h19.32  C22.952,0,24,1.048,24,2.34v19.32C24,22.952,22.952,24,21.66,24z" fill="#fff"/><path d="M51.66,24H32.34C31.048,24,30,22.952,30,21.66V2.34C30,1.048,31.048,0,32.34,0h19.32  C52.952,0,54,1.048,54,2.34v19.32C54,22.952,52.952,24,51.66,24z" fill="#fff"/><path d="M21.66,54H2.34C1.048,54,0,52.952,0,51.66V32.34C0,31.048,1.048,30,2.34,30h19.32  c1.292,0,2.34,1.048,2.34,2.34v19.32C24,52.952,22.952,54,21.66,54z" fill="#fff"/><line x1="42" x2="42" y1="30" y2="54" fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/><line x1="30" x2="54" y1="42" y2="42" fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/></svg></span></span><span>Our Apps</span></span>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <ul class="Polaris-ResourceList" aria-live="polite">
                    <li class="Polaris-ResourceList__ItemWrapper">
                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                            <div class="Polaris-ResourceList-Item__Container">
                                <div class="Polaris-ResourceList-Item__Owned">
                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall"><img src="https://apps.shopifycdn.com/listing_images/6833111235c2cb8533eecd1732d643fe/icon/e6080092a4f139fd8be69ca211d168d7.jpg" alt="Akshara In Gorgeous Blue Color Saree Product_abc_abc" class="Polaris-Thumbnail__Image"></span></div>
                                </div>
                                <div class="Polaris-ResourceList-Item__Content">
                                    <h3><span class="Polaris-TextStyle--variationStrong">Under Construction</span></h3>
                                    <div>Capture leads and make brand name viral before site goes live</div>
                                    <a href="https://apps.shopify.com/under-construction" target="_top" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Polaris-ResourceList__ItemWrapper">
                        <div class="Polaris-ResourceList-Item"><a aria-describedby="256" class="Polaris-ResourceList-Item__Link" href="customers/256" data-polaris-unstyled="true"></a>
                            <div class="Polaris-ResourceList-Item__Container">
                                <div class="Polaris-ResourceList-Item__Owned">
                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall"><img src="https://apps.shopifycdn.com/listing_images/5814587a4a574195d92a6acd49efec51/icon/c3a7cfff1cb7a5abb3d05c3f4ca6612f.png" alt="Akshara In Gorgeous Blue Color Saree Product_abc_abc" class="Polaris-Thumbnail__Image"></span></div>
                                </div>
                                <div class="Polaris-ResourceList-Item__Content">
                                    <h3><span class="Polaris-TextStyle--variationStrong">On Time Delivery</span></h3>
                                    <div>Let customers choose their delivery time and date</div>
                                    <a href="https://apps.shopify.com/under-construction" target="_top" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Polaris-ResourceList__ItemWrapper">
                        <div class="Polaris-ResourceList-Item"><a aria-describedby="256" class="Polaris-ResourceList-Item__Link" href="customers/256" data-polaris-unstyled="true"></a>
                            <div class="Polaris-ResourceList-Item__Container">
                                <div class="Polaris-ResourceList-Item__Owned">
                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall"><img src="https://apps.shopifycdn.com/listing_images/7f4e262231ff0ad9efc8cfbf5c7dda69/icon/e37ef9c1ca2e89a879a6da2f7a59e3a1.png" alt="Akshara In Gorgeous Blue Color Saree Product_abc_abc" class="Polaris-Thumbnail__Image"></span></div>
                                </div>
                                <div class="Polaris-ResourceList-Item__Content">
                                    <h3><span class="Polaris-TextStyle--variationStrong">Top Promo Bar</span></h3>
                                    <div>Simply the easiest way to add a topbar to your website</div>
                                    <a href="https://apps.shopify.com/under-construction" target="_top" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Polaris-ResourceList__ItemWrapper">
                        <div class="Polaris-ResourceList-Item"><a aria-describedby="256" class="Polaris-ResourceList-Item__Link" href="customers/256" data-polaris-unstyled="true"></a>
                            <div class="Polaris-ResourceList-Item__Container" id="256">
                                <div class="Polaris-ResourceList-Item__Owned">
                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall"><img src="https://apps.shopifycdn.com/listing_images/e3a03c0cf2fac0d30fb496ecd3c6a3fa/icon/faf04c1afc1c89d50827f09fff2ea71f.png" alt="Akshara In Gorgeous Blue Color Saree Product_abc_abc" class="Polaris-Thumbnail__Image"></span></div>
                                </div>
                                <div class="Polaris-ResourceList-Item__Content">
                                    <h3><span class="Polaris-TextStyle--variationStrong">Product Notify by Appsonrent</span></h3>
                                    <div>Make Sales With Automated Price update Notification</div>
                                    <a href="https://apps.shopify.com/under-construction" target="_top" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Polaris-ResourceList__ItemWrapper">
                        <div class="Polaris-ResourceList-Item"><a aria-describedby="256" class="Polaris-ResourceList-Item__Link" href="customers/256" data-polaris-unstyled="true"></a>
                            <div class="Polaris-ResourceList-Item__Container">
                                <div class="Polaris-ResourceList-Item__Content">
                                    <h3><span class="Polaris-Button__Content">
                                            <span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 55 55" focusable="false" aria-hidden="true"><path d="M21.66,24H2.34C1.048,24,0,22.952,0,21.66V2.34C0,1.048,1.048,0,2.34,0h19.32  C22.952,0,24,1.048,24,2.34v19.32C24,22.952,22.952,24,21.66,24z" fill="#EFCE4A"/><path d="M51.66,24H32.34C31.048,24,30,22.952,30,21.66V2.34C30,1.048,31.048,0,32.34,0h19.32  C52.952,0,54,1.048,54,2.34v19.32C54,22.952,52.952,24,51.66,24z" fill="#7FABDA"/><path d="M21.66,54H2.34C1.048,54,0,52.952,0,51.66V32.34C0,31.048,1.048,30,2.34,30h19.32  c1.292,0,2.34,1.048,2.34,2.34v19.32C24,52.952,22.952,54,21.66,54z" fill="#D75A4A"/><line x1="42" x2="42" y1="30" y2="54" fill="none" stroke="#23A24D" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/><line x1="30" x2="54" y1="42" y2="42" fill="none" stroke="#23A24D" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/></svg></span></span>
                                            <span><a href="https://apps.shopify.com/partners/appsonrent" target="_top">View All</a></span>
                                            <span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M10.707 17.707l5-5a.999.999 0 1 0-1.414-1.414L11 14.586V3a1 1 0 1 0-2 0v11.586l-3.293-3.293a.999.999 0 1 0-1.414 1.414l5 5a.999.999 0 0 0 1.414 0" fill-rule="evenodd"></path></svg></span> </span>
                                        </span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- our app dropdown block end-->
    <div class="Polaris-Page__Content">
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Rating Block</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- feedback block start-->
                                <div class="Polaris-Banner Polaris-Banner--hasDismiss Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner4Content">
                                    <div class="Polaris-Banner__Dismiss"><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span></span></span></button></div>
                                    <div
                                        class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><path fill="currentColor" d="M2 3h11v4h6l-2 4 2 4H8v-4H3"></path><path d="M16.105 11.447L17.381 14H9v-2h4a1 1 0 0 0 1-1V8h3.38l-1.274 2.552a.993.993 0 0 0 0 .895zM2.69 4H12v6H4.027L2.692 4zm15.43 7l1.774-3.553A1 1 0 0 0 19 6h-5V3c0-.554-.447-1-1-1H2.248L1.976.782a1 1 0 1 0-1.953.434l4 18a1.006 1.006 0 0 0 1.193.76 1 1 0 0 0 .76-1.194L4.47 12H7v3a1 1 0 0 0 1 1h11c.346 0 .67-.18.85-.476a.993.993 0 0 0 .044-.972l-1.775-3.553z"></path></g></svg></span></div>
                                    <div>
                                        <div class="Polaris-Banner__Content" id="Banner4Content">
                                            <p>If you have a minute, let us know what you think about Analytics. <button class="Polaris-Link">Give feedback.</button></p>
                                        </div>
                                    </div>
                                </div>
                                <!-- feedback block end-->
                                <!-- rating block start-->
                                <div class="Polaris-Banner Polaris-Banner--statusInfo Polaris-Banner--withinPage app-rating-banner" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner4Content">
                                    <div class="Polaris-Banner__Dismiss"><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span></span></span></button></div>
                                    <div class="rating-banner">
                                        <div class="Polaris-Banner__Content" id="Banner4Content">
                                            <span>How would you rate our app? <input id="rating" name="rate_score" class="hide"></span>

                                        </div>
                                    </div>
                                </div>
                                <!-- rating block end-->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">App Update & New App Release Block</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- Update app block-->
                                <div class="Polaris-Banner Polaris-Banner--statusWarning Polaris-Banner--withinPage update-app-block" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner19Heading" aria-describedby="Banner19Content">
                                    <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorGreenDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m2.293-10.707L9 10.586 7.707 9.293a1 1 0 1 0-1.414 1.414l2 2a.997.997 0 0 0 1.414 0l4-4a1 1 0 1 0-1.414-1.414"></path></g></svg></span></div>
                                    <div class="Polaris-Banner__Heading" id="Banner19Heading">
                                        <p class="Polaris-Heading">Your shipping label is ready to print.</p>
                                        <p>New version Update, We provides now Analytic result for your link. Please regenerate your link to start analytic for your old link.</p>
                                    </div>
                                </div>
                                <!--  Update app block-->
                                <div class="Polaris-Banner Polaris-Banner--withinPage Polaris-Banner--statusInfo new-app-block" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner10Content" aria-labelledby="Banner10Heading">
                                    <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><path fill="currentColor" d="M2 3h11v4h6l-2 4 2 4H8v-4H3"></path><path d="M16.105 11.447L17.381 14H9v-2h4a1 1 0 0 0 1-1V8h3.38l-1.274 2.552a.993.993 0 0 0 0 .895zM2.69 4H12v6H4.027L2.692 4zm15.43 7l1.774-3.553A1 1 0 0 0 19 6h-5V3c0-.554-.447-1-1-1H2.248L1.976.782a1 1 0 1 0-1.953.434l4 18a1.006 1.006 0 0 0 1.193.76 1 1 0 0 0 .76-1.194L4.47 12H7v3a1 1 0 0 0 1 1h11c.346 0 .67-.18.85-.476a.993.993 0 0 0 .044-.972l-1.775-3.553z"></path></g></svg></span></div>
                                    <div class="Polaris-Banner__Heading" id="Banner27Heading">
                                        <p class="Polaris-Heading">New Free App to Help You Boost Sales to a whole New Level</p>
                                        <p>Scarcity, Urgency, social proof and trust - all the powerful online sales boosting techniques in one Free App. We just released Ultimate Sales Boost</p>
                                    </div>
                                    <div class="Polaris-Banner__Content" id="Banner27Content">
                                        <div class="Polaris-Banner__Actions">
                                            <div class="Polaris-ButtonGroup">
                                                <div class="Polaris-ButtonGroup__Item">
                                                    <a href="#" target="_blank" class="app-install-btn Polaris-Button float-right"><span class="Polaris-Button__Content"><span>Get it Now</span></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Popup</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <button type="button" class="Polaris-Button Polaris-Button--primary" data-toggle="modal" data-target="#simpleModal"><span class="Polaris-Button__Content"><span>Simple popup</span></span></button><br><br>
                                <button type="button" class="Polaris-Button Polaris-Button--primary" data-toggle="modal" data-target="#silderModal"><span class="Polaris-Button__Content"><span>Silder popup</span></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once('footer.php'); ?>
    <!-- Popup start-->    
    <div class="modal  fade bd-example-modal-lg shopify-onload-popup" id="simpleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close shopify-close-btn" data-dismiss="modal" aria-label="Close"><span class="remove-icon Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 212.982 212.982"><path d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312   c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312   l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937   c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z" fill="#637381" fill-rule="evenodd"></path></svg></span></button>
                </div>
                <div class="modal-body">
                    <h5>YOU'VE JUST GOTTEN</br>YOUR FIRST SALE<br>FROM RECONVERT!</h5>
                    <p>Your success is our success! </br>If you're enjoying the app</p>
                    <div class="text-center popup-btn">
                        <button type="button" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="btn-review-text">Leave us a review</span></span></button>  
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Popup end-->
        <!-- Popup start-->
        <div class="modal  fade bd-example-modal-lg silder-popup" id="silderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close shopify-close-btn" data-dismiss="modal" aria-label="Close"><span class="remove-icon Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 212.982 212.982"><path d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312   c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312   l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937   c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z" fill="#637381" fill-rule="evenodd"></path></svg></span></button>
                    </div>
                    <div class="modal-body">
                        <div id="owl-demo" class="owl-carousel owl-theme">
                            <div class="item"><img src="assets/img/fullimage1.jpg" alt="The Last of us"></div>
                            <div class="item"><img src="assets/img/fullimage2.jpg" alt="GTA V"></div>
                            <div class="item"><img src="assets/img/fullimage3.jpg" alt="Mirror Edge"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!-- Popup end-->