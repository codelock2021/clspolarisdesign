"use strict";
/****************************
 *  SOME COMMON SVG CONSTANT *
 ****************************/
var SVG_LOADER = '<svg viewBox="0 0 20 20" class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall" aria-label="Loading" role="status"><path d="M7.229 1.173a9.25 9.25 0 1 0 11.655 11.412 1.25 1.25 0 1 0-2.4-.698 6.75 6.75 0 1 1-8.506-8.329 1.25 1.25 0 1 0-.75-2.385z"></path></svg>';
var SVG_DELETE = '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>';

var SVG_MINUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 80 80" focusable="false" aria-hidden="true"><path d="M39.769,0C17.8,0,0,17.8,0,39.768c0,21.956,17.8,39.768,39.769,39.768   c21.965,0,39.768-17.812,39.768-39.768C79.536,17.8,61.733,0,39.769,0z M13.261,45.07V34.466h53.014V45.07H13.261z" fill-rule="evenodd" fill="#DE3618"></path></svg>';
var SVG_PLUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 9h-6V3a1 1 0 1 0-2 0v6H3a1 1 0 1 0 0 2h6v6a1 1 0 1 0 2 0v-6h6a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg>';

var SVG_CIRCLE_MINUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 55 55" focusable="false" aria-hidden="true"><path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28h-25c-1.104,0-2-0.896-2-2  s0.896-2,2-2h25c1.104,0,2,0.896,2,2S39.604,28,38.5,28z" fill-rule="evenodd" fill="#DE3618"></path></svg>';
var SVG_CIRCLE_PLUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 510 510" focusable="false" aria-hidden="true"><path d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z M382.5,280.5h-102v102h-51v-102    h-102v-51h102v-102h51v102h102V280.5z" fill-rule="evenodd" fill="#3f4eae"></path></svg>';
/****************************
 *  SOME COMMON SVG CONSTANT*
 ****************************/

/****************************
 *  SOME COMMON JS FUNCTION *
 ****************************/
/****************************************
 * For Tab start
 ****************************************/
function changeTab(evt, id) {

    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("Polaris-Tabs_tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("Polaris-Tabs__Tab");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("Polaris-Tabs__Tab--selected", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(id).style.display = "block";
    evt.currentTarget.className += " Polaris-Tabs__Tab--selected";
}


/****************************************
 * For Tab end
 ****************************************/
/****************************************
 * Add And Remove item
 ****************************************/

$(document).on("click", ".addItemValue", function (e) {
    var unitNo = $(this).find('button').attr('data-uno');
    $.post("template/add-more.html", function (response, status) {
        response = response.replace(new RegExp('UBP:UNIT_ID', 'g'), unitNo);
        response = response.replace(new RegExp('UBP:WHICH_SVG_ICON', 'g'), SVG_CIRCLE_MINUS);
        response = response.replace(new RegExp('UBP:ITEM_CSS', ''), '');
        $(".optionForUnit").append(response);
        fnRemoveOptionBtn(unitNo);

    });
});
/****************************************
 * Add And Remove item
 ****************************************/

$(window).load(function ()
{
    $('#myModal').modal('show');
});
/****************************************
 * Remove Unit start
 ****************************************/
$(document).on('click', '.removeAdditem', function () {
    var unitNo = $(this).attr('data-uno');
    $(this).closest('.add-item-' + unitNo).remove();
    fnRemoveOptionBtn(unitNo);

});
/****************************************
 * hide show remove button start
 ****************************************/
function fnRemoveOptionBtn(unitNo) {
    var unitOptions = $(".removeAdditem[data-uno='" + unitNo + "']");
    /* for remove button hide show*/
    //console.log('total option = ' + unitOptions.length);
    if (unitOptions != undefined && unitOptions.length > 1) {
        unitOptions.show();
    } else {
        unitOptions.hide();
    }

}
/****************************************
 * hide show remove button end
 ****************************************/
$(document).ready(function () {

    /****************************************
     * SETTING PAGE 
     ****************************************/
//    rating js start
    $('#rating').rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'xs',
        showClear: false
    });
//    rating js end
    /*
     * For color picker 
     */
    $(".spectrumColor").spectrum({
        showButtons: false,
        //showAlpha: true
    });

    $(".spectrumColor").on('move.spectrum', function (e, color) {
        showAlpha: true
        var id = $(this).data('id');
        var hexVal = color.toRgbString();
        $("[data-id='" + id + "']").val(hexVal);
    });
    /*Transparent colorpicker Start*/
    $(".spectrumTransparentColor").spectrum({
        showButtons: false,
        showAlpha: true
    });

    $(".spectrumTransparentColor").on('move.spectrum', function (e, color) {
        showAlpha: true
        var id = $(this).data('id');
        var hexVal = color.toRgbString();
        $("[data-id='" + id + "']").val(hexVal);
    });
    /*Transparent colorpicker End*/
    /*
     * End of color picker 
     */

    /*
     * popver start
     */
    $('[data-toggle="popover"]').each(function (i, obj) {
        $(this).popover({
            html: true,
            content: function () {
                var id = $(this).attr('id')
                return $('#popover-content-' + id).html();
            }
        });

    });
    /*
     * popver start end 
     */
    /*
     * Owl carousel start
     */
    var owl = $('#footerSilder');
    owl.owlCarousel({
        loop: true,
        //autoplay: true,
        margin: 20,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            960: {
                items: 2
            },
            1200: {
                items: 3
            },
            1600: {
                items: 3
            }
        }
    });
    $(".next").click(function () {
        //alert('1');
        owl.trigger('next.owl.carousel');
    })
    $(".prev").click(function () {
        owl.trigger('prev.owl.carousel');
    })
    /*
     * Owl carousel end
     */
    /*Select2 js start*/
    $('.countrySelect2').select2();
    /*select2 js end*/
    
        /*
     * Owl carousel start
     */
        var owl = $('#owl-demo');
        owl.owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            dots: true,
            nav:true,
           items: 1
      });
    /*
     * Owl carousel end
     */

    /****************************************
     * End of SETTING PAGE 
     ****************************************/
});

/****************************************
 * Number js Start
 ****************************************/
$(document).on('click', '.down', function () {
    var value = $("#myNumber").val();
    if (value != '') {
        value = parseInt(value) - 1;
    } else {
        value = -1;
    }
    $("#myNumber").val(value)

//    if (document.getElementById("myNumber").value <= parseInt(min)) {
//        document.getElementById("myNumber").value = min;
//    }
});
$(document).on('click', '.up', function () {
    var value = $("#myNumber").val();
    if (value != '') {
        value = parseInt(value) + 1;
    } else {
        value = 0;

    }
    $("#myNumber").val(value)
//    if (document.getElementById("myNumber").value >= parseInt(max)) {
//        document.getElementById("myNumber").value = max;
//    }
});
/****************************************
 * Number js End
 ****************************************/
/****************************************
 * Menu Active class js start
 ****************************************/
$(function () {
    var pgurl = window.location.href.substr(window.location.href
            .lastIndexOf("/") + 1);
    $("ul.menu-ul li a").each(function () {
        if ($(this).attr("href") == pgurl || $(this).attr("href") == '')
            $(this).addClass("active");
    })

});
/****************************************
 * Menu Active class js end
 ****************************************/

/****************************************
 * input Daterange Picker Start
 ****************************************/
$(function () {
    $('input[name="daterange"]').val(new Date());
    $('.report-daterangepicker').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "alwaysShowCalendars": true,
        "startDate": new Date(),
        "endDate": new Date(),
        "maxDate": new Date(),
        "opens": "center"
    }, function (start, end, label) {
        console.log(label);
        $('input[name="daterange"]').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));

    });
});
/****************************************
 * input Daterange Picker End
 ****************************************/
/****************************************
 * Button Daterange Picker Start
 ****************************************/
$(function () {
    $('.btn-report-daterangepicker').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        },
        ranges: {
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "alwaysShowCalendars": true,
        "startDate": new Date(),
        "endDate": new Date(),
        "maxDate": new Date(),
        "opens": "right"
    }, function (start, end, label) {
        if (label == 'Custom Range') {
            $('#dateRangeSpan').text(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        } else {
            $('#dateRangeSpan').text(label);
        }
    });
});
/****************************************
 * Button Daterange Picker End
 ****************************************/
/*Single DateRange Picker Start*/
$(function () {
    $('input[name="single-daterange"]').val(new Date());
    $('.input-single-daterangepicker').daterangepicker({
        "singleDatePicker": true,
        "maxDate": new Date(),
    }, function (start, end, label) {
        //console.log(start);
        $('input[name="single-daterange"]').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));

    });
});
function preview_image()
{
    var total_file = document.getElementById("upload_file").files.length;
    for (var i = 0; i < total_file; i++)
    {
        $('#image_preview').append("<img src='" + URL.createObjectURL(event.target.files[i]) + "'><br>");
    }
}
/*Single DateRange Picker End*/