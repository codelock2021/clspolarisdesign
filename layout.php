<?php include_once('header.php'); ?>
<?php include_once('sidebar.php'); ?>
<div class="Polaris-Page Polaris-Page--fullWidth">
    <div class="Polaris-Page__Content">
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Online store dashboard</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- feedback block start-->
                                <div class="Polaris-Banner Polaris-Banner--hasDismiss Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner4Content">
                                    <div class="Polaris-Banner__Dismiss"><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span></span></span></button></div>
                                    <div
                                        class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><path fill="currentColor" d="M2 3h11v4h6l-2 4 2 4H8v-4H3"></path><path d="M16.105 11.447L17.381 14H9v-2h4a1 1 0 0 0 1-1V8h3.38l-1.274 2.552a.993.993 0 0 0 0 .895zM2.69 4H12v6H4.027L2.692 4zm15.43 7l1.774-3.553A1 1 0 0 0 19 6h-5V3c0-.554-.447-1-1-1H2.248L1.976.782a1 1 0 1 0-1.953.434l4 18a1.006 1.006 0 0 0 1.193.76 1 1 0 0 0 .76-1.194L4.47 12H7v3a1 1 0 0 0 1 1h11c.346 0 .67-.18.85-.476a.993.993 0 0 0 .044-.972l-1.775-3.553z"></path></g></svg></span></div>
                                    <div>
                                        <div class="Polaris-Banner__Content" id="Banner4Content">
                                            <p>If you have a minute, let us know what you think about Analytics. <button class="Polaris-Link">Give feedback.</button></p>
                                        </div>
                                    </div>
                                </div>
                                <!-- feedback block end-->
                                <!-- rating block start-->
                                <div class="Polaris-Banner Polaris-Banner--statusInfo Polaris-Banner--withinPage app-rating-banner" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner4Content">
                                    <div class="Polaris-Banner__Dismiss"><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span></span></span></button></div>
                                    <div class="rating-banner">
                                        <div class="Polaris-Banner__Content" id="Banner4Content">
                                            <span>How would you rate our app? <input id="rating" name="rate_score" class="hide"></span>

                                        </div>
                                    </div>
                                </div>
                                <!-- rating block end-->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">One-column layout</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- one column layout start-->
                                <div class="Polaris-Layout">
                                    <div class="Polaris-Layout__Section">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <h2 class="Polaris-Heading">One column layout</h2>
                                            </div>
                                            <div class="Polaris-Card__Section">
                                                <p>View a summary of your online store’s performance.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- one column layout end-->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Two columns with primary and secondary width</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- Two column layout start-->
                                <div class="Polaris-Layout">
                                    <div class="Polaris-Layout__Section">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <h2 class="Polaris-Heading">Order details</h2>
                                            </div>
                                            <div class="Polaris-Card__Section">
                                                <p>View a summary of your order.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Polaris-Layout__Section Polaris-Layout__Section--secondary">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <h2 class="Polaris-Heading">Tags</h2>
                                            </div>
                                            <div class="Polaris-Card__Section">
                                                <p>Add tags to your order.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Two column layout end-->
                            </div>
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Two columns with equal width</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- Two columns with equal width start-->
                                <div class="Polaris-Layout">
                                    <div class="Polaris-Layout__Section Polaris-Layout__Section--secondary">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                                        <h2 class="Polaris-Heading">Florida</h2>
                                                    </div>
                                                    <div class="Polaris-Stack__Item">
                                                        <div class="Polaris-ButtonGroup">
                                                            <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain"><button type="button" class="Polaris-Button Polaris-Button--plain"><span class="Polaris-Button__Content"><span>Manage</span></span></button></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Polaris-Card__Section"><span class="Polaris-TextStyle--variationSubdued">455 units available</span></div>
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-Card__SectionHeader">
                                                    <h3 aria-label="Items" class="Polaris-Subheading">Items</h3>
                                                </div>
                                                <div class="Polaris-ResourceList__ResourceListWrapper">
                                                    <ul class="Polaris-ResourceList" aria-live="polite">
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Black &amp; orange scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/341" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="341">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/black-orange-stripes_373x@2x.jpg" alt="Black orange scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Black &amp; orange scarf</b></h3>
                                                                        <div>SKU: 9234194023</div>
                                                                        <div>254 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="256" aria-label="View details for Tucan scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/256" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="256">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/tucan-scarf_373x@2x.jpg" alt="Tucan scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Tucan scarf</b></h3>
                                                                        <div>SKU: 9234194010</div>
                                                                        <div>201 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Polaris-Layout__Section Polaris-Layout__Section--secondary">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                                        <h2 class="Polaris-Heading">Nevada</h2>
                                                    </div>
                                                    <div class="Polaris-Stack__Item">
                                                        <div class="Polaris-ButtonGroup">
                                                            <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain"><button type="button" class="Polaris-Button Polaris-Button--plain"><span class="Polaris-Button__Content"><span>Manage</span></span></button></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Polaris-Card__Section"><span class="Polaris-TextStyle--variationSubdued">301 units available</span></div>
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-Card__SectionHeader">
                                                    <h3 aria-label="Items" class="Polaris-Subheading">Items</h3>
                                                </div>
                                                <div class="Polaris-ResourceList__ResourceListWrapper">
                                                    <ul class="Polaris-ResourceList" aria-live="polite">
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Black &amp; orange scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/341" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="341">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/black-orange-stripes_373x@2x.jpg" alt="Black orange scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Black &amp; orange scarf</b></h3>
                                                                        <div>SKU: 9234194023</div>
                                                                        <div>100 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="256" aria-label="View details for Tucan scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/256" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="256">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/tucan-scarf_373x@2x.jpg" alt="Tucan scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Tucan scarf</b></h3>
                                                                        <div>SKU: 9234194010</div>
                                                                        <div>201 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Two columns with equal width end-->
                            </div>
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Three columns with equal width</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- Three columns with equal width start-->
                                <div class="Polaris-Layout">
                                    <div class="Polaris-Layout__Section Polaris-Layout__Section--secondary">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                                        <h2 class="Polaris-Heading">Florida</h2>
                                                    </div>
                                                    <div class="Polaris-Stack__Item">
                                                        <div class="Polaris-ButtonGroup">
                                                            <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain"><button type="button" class="Polaris-Button Polaris-Button--plain"><span class="Polaris-Button__Content"><span>Manage</span></span></button></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Polaris-Card__Section"><span class="Polaris-TextStyle--variationSubdued">455 units available</span></div>
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-Card__SectionHeader">
                                                    <h3 aria-label="Items" class="Polaris-Subheading">Items</h3>
                                                </div>
                                                <div class="Polaris-ResourceList__ResourceListWrapper">
                                                    <ul class="Polaris-ResourceList" aria-live="polite">
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Black &amp; orange scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/341" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="341">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/black-orange-stripes_373x@2x.jpg" alt="Black orange scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Black &amp; orange scarf</b></h3>
                                                                        <div>SKU: 9234194023</div>
                                                                        <div>254 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="256" aria-label="View details for Tucan scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/256" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="256">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/tucan-scarf_373x@2x.jpg" alt="Tucan scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Tucan scarf</b></h3>
                                                                        <div>SKU: 9234194010</div>
                                                                        <div>201 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Polaris-Layout__Section Polaris-Layout__Section--secondary">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                                        <h2 class="Polaris-Heading">Nevada</h2>
                                                    </div>
                                                    <div class="Polaris-Stack__Item">
                                                        <div class="Polaris-ButtonGroup">
                                                            <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain"><button type="button" class="Polaris-Button Polaris-Button--plain"><span class="Polaris-Button__Content"><span>Manage</span></span></button></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Polaris-Card__Section"><span class="Polaris-TextStyle--variationSubdued">301 units available</span></div>
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-Card__SectionHeader">
                                                    <h3 aria-label="Items" class="Polaris-Subheading">Items</h3>
                                                </div>
                                                <div class="Polaris-ResourceList__ResourceListWrapper">
                                                    <ul class="Polaris-ResourceList" aria-live="polite">
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Black &amp; orange scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/341" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="341">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/black-orange-stripes_373x@2x.jpg" alt="Black orange scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Black &amp; orange scarf</b></h3>
                                                                        <div>SKU: 9234194023</div>
                                                                        <div>100 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="256" aria-label="View details for Tucan scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/256" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="256">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/tucan-scarf_373x@2x.jpg" alt="Tucan scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Tucan scarf</b></h3>
                                                                        <div>SKU: 9234194010</div>
                                                                        <div>201 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Polaris-Layout__Section Polaris-Layout__Section--secondary">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                                        <h2 class="Polaris-Heading">Minneapolis</h2>
                                                    </div>
                                                    <div class="Polaris-Stack__Item">
                                                        <div class="Polaris-ButtonGroup">
                                                            <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain"><button type="button" class="Polaris-Button Polaris-Button--plain"><span class="Polaris-Button__Content"><span>Manage</span></span></button></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Polaris-Card__Section"><span class="Polaris-TextStyle--variationSubdued">1931 units available</span></div>
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-Card__SectionHeader">
                                                    <h3 aria-label="Items" class="Polaris-Subheading">Items</h3>
                                                </div>
                                                <div class="Polaris-ResourceList__ResourceListWrapper">
                                                    <ul class="Polaris-ResourceList" aria-live="polite">
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Black &amp; orange scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/341" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="341">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/black-orange-stripes_373x@2x.jpg" alt="Black orange scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Black &amp; orange scarf</b></h3>
                                                                        <div>SKU: 9234194023</div>
                                                                        <div>1230 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="Polaris-ResourceList__ItemWrapper" tabindex="0">
                                                            <div class="Polaris-ResourceList-Item"><a aria-describedby="256" aria-label="View details for Tucan scarf" class="Polaris-ResourceList-Item__Link" href="produdcts/256" data-polaris-unstyled="true"></a>
                                                                <div class="Polaris-ResourceList-Item__Container" id="256">
                                                                    <div class="Polaris-ResourceList-Item__Owned">
                                                                        <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium"><img src="https://burst.shopifycdn.com/photos/tucan-scarf_373x@2x.jpg" alt="Tucan scarf" class="Polaris-Thumbnail__Image"></span></div>
                                                                    </div>
                                                                    <div class="Polaris-ResourceList-Item__Content">
                                                                        <h3><b class="Polaris-TextStyle--variationStrong">Tucan scarf</b></h3>
                                                                        <div>SKU: 9234194010</div>
                                                                        <div>701 available</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Three columns with equal width end-->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Annotated layout</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- one column layout start-->
                                <div class="Polaris-Layout">
                                    <div class="Polaris-Layout__AnnotatedSection">
                                        <div class="Polaris-Layout__AnnotationWrapper">
                                            <div class="Polaris-Layout__Annotation">
                                                <div class="Polaris-TextContainer">
                                                    <h2 class="Polaris-Heading">Store details</h2>
                                                    <div class="Polaris-Layout__AnnotationDescription">
                                                        <p>Shopify and your customers will use this information to contact you.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="Polaris-Layout__AnnotationContent">
                                                <div class="Polaris-Card">
                                                    <div class="Polaris-Card__Section">
                                                        <div class="Polaris-FormLayout">
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="">
                                                                    <div class="Polaris-Labelled__LabelWrapper">
                                                                        <div class="Polaris-Label"><label id="TextField13Label" for="TextField13" class="Polaris-Label__Text">Store name</label></div>
                                                                    </div>
                                                                    <div class="Polaris-TextField"><input id="TextField13" class="Polaris-TextField__Input" aria-labelledby="TextField13Label" aria-invalid="false" value="">
                                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="">
                                                                    <div class="Polaris-Labelled__LabelWrapper">
                                                                        <div class="Polaris-Label"><label id="TextField14Label" for="TextField14" class="Polaris-Label__Text">Account email</label></div>
                                                                    </div>
                                                                    <div class="Polaris-TextField"><input id="TextField14" class="Polaris-TextField__Input" type="email" aria-labelledby="TextField14Label" aria-invalid="false" value="">
                                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- one column layout end-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once('footer.php'); ?>