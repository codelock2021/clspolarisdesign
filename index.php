<?php include_once('header.php'); ?>
<?php include_once('sidebar.php'); ?>
<div class="Polaris-Page Polaris-Page--fullWidth">
    <div class="Polaris-Page__Content">
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Full Page <code>(Polaris-Page--fullWidth)</code> </h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- Full Width page code start-->
                                <div class="Polaris-Page Polaris-Page--fullWidth">
                                    <div class="Polaris-Page__Content">
                                        <div class="Polaris-Layout">
                                            <div class="Polaris-Layout__AnnotatedSection">
                                                <div class="Polaris-Layout__AnnotationWrapper">
                                                    <div class="Polaris-Layout__AnnotationContent">
                                                        <div class="Polaris-Card">
                                                            <div class="Polaris-Card__Header">
                                                                <h2 class="Polaris-Heading">Online store dashboard</h2>
                                                            </div>
                                                            <div class="Polaris-Card__Section">
                                                                <p>View a summary of your online store’s performance.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Full Width page code end-->
                            </div>
                            <div class="Polaris-Card__Section">
                                <div class="Polaris-Card__SectionHeader">
                                    <h3 class="Polaris-Heading">Only Page  <code>(Polaris-Page)</code> </h3>
                                </div>
                                <!-- Page code start-->
                                <div class="Polaris-Page">
                                    <div class="Polaris-Page__Content">
                                        <div class="Polaris-Layout">
                                            <div class="Polaris-Layout__AnnotatedSection">
                                                <div class="Polaris-Layout__AnnotationWrapper">
                                                    <div class="Polaris-Layout__AnnotationContent">
                                                        <div class="Polaris-Card">
                                                            <div class="Polaris-Card__Header">
                                                                <h2 class="Polaris-Heading">Online store dashboard</h2>
                                                            </div>
                                                            <div class="Polaris-Card__Section">
                                                                <p>View a summary of your online store’s performance.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Page code end -->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Table</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <div class="Polaris-Page__MainContent">
                                    <div class="Polaris-Page__TitleAndActions">
                                        <div class="Polaris-Page__Title">
                                            <div class="Polaris-Connected">
                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                    <div class="Polaris-TextField Polaris-TextField--hasValue">
                                                        <div class="Polaris-TextField__Prefix"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                        <input type="text" id="search" name="search" class="Polaris-TextField__Input browse_buy_product_search" aria-invalid="false" placeholder="Search name" autocomplete="off">
                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="Polaris-Button Polaris-Button--primary ml-4" href="#"><span class="Polaris-Button__Content"><span>Demo</span></span></a>
                                </div>
                                <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                <!-- Table Code Start-->
                                <div class="Polaris-Card mt-5">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Price</th>
                                                    <th>Sku</th>
                                                    <th class="w-25">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="Polaris-ResourceList__ItemWrapper">
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>
                                                        <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="View"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                            <div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Edit"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></span></span></a></div>
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Delete"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="Polaris-ResourceList__ItemWrapper">
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>
                                                        <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="View"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                            <div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Edit"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></span></span></a></div>
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Delete"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="Polaris-ResourceList__ItemWrapper">
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>
                                                        <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="View"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                            <div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Edit"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></span></span></a></div>
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Delete"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="Polaris-ResourceList__ItemWrapper">
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>
                                                        <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="View"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                            <div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Edit"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></span></span></a></div>
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Delete"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Pagination block start-->
                                    <div class="page-pagination mb-4" id="">
                                        <div class="Polaris-Page__Pagination">
                                            <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                                                <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                    <div class="Polaris-ButtonGroup__Item">
                                                        <a href="javascript:void(0)" class="Polaris-Button tip display_inline_block Polaris-Button--disabled" data-hover="Previous">
                                                            <span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20">
                                                                        <path d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div class="Polaris-ButtonGroup__Item">
                                                        <a href="javascript:void(0)" class="Polaris-Button display_inline_block tip " data-hover="Next">
                                                            <span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20">
                                                                        <path d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </nav>
                                        </div>
                                    </div>
                                    <!--Pagination block end-->
                                </div>
                                <!--Table Code End-->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Table (Striped rows)</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <div class="Polaris-Page__MainContent">
                                    <div class="Polaris-Page__TitleAndActions">
                                        <div class="Polaris-Page__Title">
                                            <div class="Polaris-Connected">
                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                    <div class="Polaris-TextField Polaris-TextField--hasValue">
                                                        <div class="Polaris-TextField__Prefix"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                        <input type="text" id="search" name="search" class="Polaris-TextField__Input browse_buy_product_search" aria-invalid="false" placeholder="Search name" autocomplete="off">
                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="Polaris-Button Polaris-Button--primary ml-4" href="#"><span class="Polaris-Button__Content"><span>Demo</span></span></a>
                                </div>
                                <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                <!-- Table Code Start-->
                                <div class="Polaris-Card mt-5">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Price</th>
                                                    <th>Sku</th>
                                                    <th class="w-25">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="Polaris-ResourceList__ItemWrapper">
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>
                                                        <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="View"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                            <div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Edit"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></span></span></a></div>
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Delete"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="Polaris-ResourceList__ItemWrapper">
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>
                                                        <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="View"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                            <div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Edit"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></span></span></a></div>
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Delete"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="Polaris-ResourceList__ItemWrapper">
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>
                                                        <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="View"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                            <div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Edit"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></span></span></a></div>
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Delete"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="Polaris-ResourceList__ItemWrapper">
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>Demo</td>
                                                    <td>
                                                        <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="View"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                            <div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Edit"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></span></span></a></div>
                                                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content tip" data-hover="Delete"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></span></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Pagination block start-->
                                    <div class="page-pagination mb-4" id="">
                                        <div class="Polaris-Page__Pagination">
                                            <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                                                <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                    <div class="Polaris-ButtonGroup__Item">
                                                        <a href="javascript:void(0)" class="Polaris-Button tip display_inline_block Polaris-Button--disabled" data-hover="Previous">
                                                            <span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20">
                                                                        <path d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div class="Polaris-ButtonGroup__Item">
                                                        <a href="javascript:void(0)" class="Polaris-Button display_inline_block tip " data-hover="Next">
                                                            <span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20">
                                                                        <path d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </nav>
                                        </div>
                                    </div>
                                    <!--Pagination block end-->
                                </div>
                                <!--Table Code End-->
                            </div>
                        </div>
                        <!--List And Filtering Start-->
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">List with filtering</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <div class="Polaris-Card">
                                    <div class="Polaris-ResourceList__ResourceListWrapper">
                                        <div class="Polaris-ResourceList__FiltersWrapper">
                                            <div class="Polaris-FormLayout">
                                                <div class="Polaris-FormLayout__Item">
                                                    <div class="Polaris-Labelled--hidden">
                                                        <div class="Polaris-Labelled__LabelWrapper">
                                                            <div class="Polaris-Label"><label id="TextField1Label" for="TextField1" class="Polaris-Label__Text">Search customers</label></div>
                                                        </div>
                                                        <div class="Polaris-Connected">
                                                            <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                                <div class="Polaris-TextField Polaris-TextField--hasValue">
                                                                    <div class="Polaris-TextField__Prefix"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                                    <input type="text" id="search" name="search" class="Polaris-TextField__Input browse_buy_product_search" aria-invalid="false" placeholder="Search name" autocomplete="off">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                            </div>
                                                            <div class="Polaris-Connected__Item Polaris-Connected__Item--connection">
                                                                <div class="Polaris-Labelled--hidden">
                                                                    <button type="button" class="Polaris-Button browse-customer" data-toggle="modal" data-target="#add-product-popup"><span class="Polaris-Button__Content"><span>Browse</span></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="Polaris-ResourceList" aria-live="polite">
                                            <li class="Polaris-ResourceList__ItemWrapper">
                                                <div class="Polaris-ResourceList-Item"><a aria-describedby="341" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                                    <div class="Polaris-ResourceList-Item__Container" id="341">
                                                        <div class="Polaris-ResourceList-Item__Owned">
                                                            <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall"><img src="https://cdn.shopify.com/s/files/1/1040/8898/products/clothes-product-1.png?v=1499412844" alt="Akshara In Gorgeous Blue Color Saree Product_abc_abc" class="Polaris-Thumbnail__Image"></span></div>
                                                        </div>
                                                        <div class="Polaris-ResourceList-Item__Content">
                                                            <h3><span class="Polaris-TextStyle--variationStrong">Akshara In Gorgeous Blue Color Saree Product_abc_abc</span></h3>
                                                            <div>Decatur, USA</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="Polaris-ResourceList__ItemWrapper">
                                                <div class="Polaris-ResourceList-Item"><a aria-describedby="256" class="Polaris-ResourceList-Item__Link" href="customers/256" data-polaris-unstyled="true"></a>
                                                    <div class="Polaris-ResourceList-Item__Container" id="256">
                                                        <div class="Polaris-ResourceList-Item__Owned">
                                                            <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall"><img src="https://cdn.shopify.com/s/files/1/1040/8898/products/clothes-product-1.png?v=1499412844" alt="Akshara In Gorgeous Blue Color Saree Product_abc_abc" class="Polaris-Thumbnail__Image"></span></div>
                                                        </div>
                                                        <div class="Polaris-ResourceList-Item__Content">
                                                            <h3><span class="Polaris-TextStyle--variationStrong">Akshara In Gorgeous Green Color saree Product_abc_abc</span></h3>
                                                            <div>Los Angeles, USA</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--List And Filtering End-->
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Bulleted List</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <ul class="Polaris-List Polaris-List--typeBullet">
                                    <li class="Polaris-List__Item">Yellow shirt</li>
                                    <li class="Polaris-List__Item">Red shirt</li>
                                    <li class="Polaris-List__Item">Green shirt</li>
                                </ul>
                                <ol class="Polaris-List Polaris-List--typeNumber">
                                    <li class="Polaris-List__Item">First item</li>
                                    <li class="Polaris-List__Item">Second item</li>
                                    <li class="Polaris-List__Item">Third Item</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                
    <?php include_once('footer.php'); ?>