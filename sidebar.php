<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
 <div class="wrapper-main menu-default menu">  
    <div class="top-menu">
        <div>
            <div style="padding-bottom: 56px;"></div>
            <div style="position: fixed; top: 0px; left: 0px; width: 1903px;">
                <div class="top-background">
                    <div class="top-icon-class">
                        <div class="_2RlBO">
                            <a class="_1dgV1" href="/admin">
                                <img src="assets/img/download.svg" alt="Shopify" class="_1w7jI">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mMV0-">
        <div class="_1COwL">
            <div class="_3wBa8"><img src="assets/img/logo.svg" class="_1gIv9" alt=""></div>
            <div class="_3-ipZ">
                <h2 class="_1-WrP"></h2>
            </div>
        </div>
    </div>
    <div class="_2HR_1"></div>
        <div class="sub-menu">
            <nav class="menu-nav">
                <ul class="menu-ul">
                    <li class="menu-li">
                        <a class="menu-active" href="index.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M19 20h-18c-.553 0-1-.447-1-1v-18c0-.553.447-1 1-1h18c.553 0 1 .447 1 1v18c0 .553-.447 1-1 1zm-17-2h16v-16h-16v16zM14.925 7h-5.925c-.553 0-1-.447-1-1s.447-1 1-1h5.925c.553 0 1 .447 1 1s-.447 1-1 1z" fill-rule="evenodd"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Table</span>
                        </a>
                    </li>
                    <li class="menu-li">
                        <a class="menu-active" href="form.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17 18h-2c-.552 0-1-.448-1-1v-14c0-.552.448-1 1-1h2c.553 0 1-.447 1-1s-.447-1-1-1h-2c-.768 0-1.469.29-2 .766-.531-.476-1.232-.766-2-.766h-2c-.553 0-1 .447-1 1s.447 1 1 1h2c.552 0 1 .448 1 1v14c0 .552-.448 1-1 1h-2c-.553 0-1 .447-1 1s.447 1 1 1h2c.768 0 1.469-.29 2-.766.531.476 1.232.766 2 .766h2c.553 0 1-.447 1-1s-.447-1-1-1zM10 15c0-.553-.447-1-1-1h-7v-8h7c.553 0 1-.447 1-1s-.447-1-1-1h-8c-.553 0-1 .447-1 1v10c0 .553.447 1 1 1h8c.553 0 1-.447 1-1zM19 4h-2c-.553 0-1 .447-1 1s.447 1 1 1h1v8h-1c-.553 0-1 .447-1 1s.447 1 1 1h2c.553 0 1-.447 1-1v-10c0-.553-.447-1-1-1z" fill-rule="evenodd"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Form</span>
                        </a>
                    </li>
                    <li class="menu-li">
                        <a class="menu-active" href="action.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M15 10c2.757 0 5-2.243 5-5s-2.243-5-5-5h-10c-2.757 0-5 2.243-5 5s2.243 5 5 5h1v2.971c-2.184-1.215-3.959-.426-4.707.322-.283.283-.37.707-.222 1.078l2 5c.153.38.52.629.929.629h12c.489 0 .906-.354.986-.836l1-6c.081-.485-.203-.957-.67-1.112l-5.316-1.773v-.279h3zm-10-2c-1.654 0-3-1.346-3-3s1.346-3 3-3h10c1.654 0 3 1.346 3 3s-1.346 3-3 3h-3v-1c0-1.654-1.346-3-3-3s-3 1.346-3 3v1h-1zm10.873 5.679l-.72 4.321h-10.476l-1.431-3.577c.59-.18 1.628-.133 3.047 1.284.286.287.716.374 1.09.217.374-.154.617-.52.617-.924v-8c0-.552.448-1 1-1s1 .448 1 1v4c0 .431.275.812.684.948l5.189 1.731z" fill-rule="evenodd"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Actions</span>
                        </a>
                    </li>
                    <li class="menu-li">
                        <a class="menu-active" href="svg.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628C17.836 9.399 15.611 4 9.999 4S2.162 9.399 2.07 9.628a1.017 1.017 0 0 0 0 .744C2.162 10.601 4.387 16 9.999 16s7.837-5.399 7.929-5.628a1.017 1.017 0 0 0 0-.744zM9.999 14a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-6A2 2 0 1 0 10 12.001 2 2 0 0 0 10 8z" fill-rule="evenodd"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Svg List</span>
                        </a>
                    </li>
                    <li class="menu-li">
                        <a class="menu-active" href="help.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><circle cx="10" cy="10" r="9" fill="currentColor"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m0-4a1 1 0 1 0 0 2 1 1 0 1 0 0-2m0-10C8.346 4 7 5.346 7 7a1 1 0 1 0 2 0 1.001 1.001 0 1 1 1.591.808C9.58 8.548 9 9.616 9 10.737V11a1 1 0 1 0 2 0v-.263c0-.653.484-1.105.773-1.317A3.013 3.013 0 0 0 13 7c0-1.654-1.346-3-3-3"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Help</span>
                        </a>
                    </li>
                    <li class="menu-li">
                        <a class="menu-active" href="layout.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><circle cx="10" cy="10" r="9" fill="currentColor"></circle><path d="M19 0h-18c-.553 0-1 .447-1 1v18c0 .553.447 1 1 1h18c.553 0 1-.447 1-1v-18c0-.553-.447-1-1-1zm-1 2v4h-16v-4h16zm-16 6h4v10h-4v-10zm6 10v-10h10v10h-10z"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Layout</span>
                        </a>
                    </li>
                    <li class="menu-li">
                        <a class="menu-active" href="Bootstrap-class-list.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 474 474" focusable="false" aria-hidden="true"><circle cx="10" cy="10" r="9" fill="currentColor"></circle><path d="M395.655,249.236c-11.037-14.272-27.692-24.075-49.964-29.403c28.362-14.467,40.826-39.021,37.404-73.666   c-1.144-12.563-4.616-23.451-10.424-32.68c-5.812-9.231-13.655-16.652-23.559-22.266c-9.896-5.621-20.659-9.9-32.264-12.85   c-11.608-2.95-24.935-5.092-39.972-6.423V0h-43.964v69.949c-7.613,0-19.223,0.19-34.829,0.571V0h-43.97v71.948   c-6.283,0.191-15.513,0.288-27.694,0.288l-60.526-0.288v46.824h31.689c14.466,0,22.936,6.473,25.41,19.414v81.942   c1.906,0,3.427,0.098,4.57,0.288h-4.57v114.769c-1.521,9.705-7.04,14.562-16.558,14.562H74.747l-8.852,52.249h57.102   c3.617,0,8.848,0.048,15.703,0.134c6.851,0.096,11.988,0.144,15.415,0.144v72.803h43.977v-71.947   c7.992,0.195,19.602,0.288,34.829,0.288v71.659h43.965v-72.803c15.611-0.76,29.457-2.18,41.538-4.281   c12.087-2.101,23.653-5.379,34.69-9.855c11.036-4.47,20.266-10.041,27.688-16.703c7.426-6.656,13.559-15.13,18.421-25.41   c4.846-10.28,7.943-22.176,9.271-35.693C410.979,283.882,406.694,263.514,395.655,249.236z M198.938,121.904   c1.333,0,5.092-0.048,11.278-0.144c6.189-0.098,11.326-0.192,15.418-0.288c4.093-0.094,9.613,0.144,16.563,0.715   c6.947,0.571,12.799,1.334,17.556,2.284s9.996,2.521,15.701,4.71c5.715,2.187,10.28,4.853,13.702,7.993   c3.429,3.14,6.331,7.139,8.706,11.993c2.382,4.853,3.572,10.42,3.572,16.7c0,5.33-0.855,10.185-2.566,14.565   c-1.708,4.377-4.284,8.042-7.706,10.992c-3.423,2.951-6.951,5.523-10.568,7.71c-3.613,2.187-8.233,3.949-13.846,5.28   c-5.612,1.333-10.513,2.38-14.698,3.14c-4.188,0.762-9.421,1.287-15.703,1.571c-6.283,0.284-11.043,0.478-14.277,0.572   c-3.237,0.094-7.661,0.094-13.278,0c-5.618-0.094-8.897-0.144-9.851-0.144v-87.65H198.938z M318.998,316.331   c-1.813,4.38-4.141,8.189-6.994,11.427c-2.858,3.23-6.619,6.088-11.28,8.559c-4.66,2.478-9.185,4.473-13.559,5.996   c-4.38,1.529-9.664,2.854-15.844,4c-6.194,1.143-11.615,1.947-16.283,2.426c-4.661,0.477-10.226,0.856-16.7,1.144   c-6.469,0.28-11.516,0.425-15.131,0.425c-3.617,0-8.186-0.052-13.706-0.145c-5.523-0.089-9.041-0.137-10.565-0.137v-96.505   c1.521,0,6.042-0.093,13.562-0.287c7.521-0.192,13.656-0.281,18.415-0.281c4.758,0,11.327,0.281,19.705,0.856   c8.37,0.567,15.413,1.42,21.128,2.562c5.708,1.144,11.937,2.902,18.699,5.284c6.755,2.378,12.23,5.28,16.419,8.706   c4.188,3.432,7.707,7.803,10.561,13.134c2.861,5.328,4.288,11.42,4.288,18.274C321.712,307.104,320.809,311.95,318.998,316.331z"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Bootstrap class list</span>
                        </a>
                    </li>
                    <li class="menu-li">
                        <a class="menu-active" href="chart.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><circle cx="10" cy="10" r="9" fill="currentColor"></circle><path d="M19 18a1 1 0 0 1 0 2H1a1 1 0 0 1 0-2h18zm0-18a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h5V5a1 1 0 0 1 1-1h5V1a1 1 0 0 1 1-1h6zm-5 14h4V2h-4v12zm-6 0h4V6H8v8zm-6 0h4v-4H2v4z"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Chart</span>
                        </a>
                    </li>
                    <li class="menu-li">
                        <a class="menu-active" href="header-commponents.php">
                            <div class="icon-svg">
                                <span class="menu-svg-icon">
                                    <svg class="Polaris-Icon__Svg" viewBox="0 0 93.658 93.658" focusable="false" aria-hidden="true"><circle cx="10" cy="10" r="9" fill="currentColor"></circle><path d="M0,1.662v90.334h93.658V1.662H0z M27.175,85.956H6.038V7.702h21.137V85.956z M59.186,85.956h-25.97V71.198h25.97V85.956   L59.186,85.956z M59.186,64.549h-25.97V49.908h25.97V64.549L59.186,64.549z M59.186,43.75h-25.97V29.109h25.97V43.75L59.186,43.75z    M87.618,85.956H65.225V71.198h22.395L87.618,85.956L87.618,85.956z M87.618,64.549H65.225V49.908h22.395L87.618,64.549   L87.618,64.549z M87.618,43.75H65.225V29.109h22.395L87.618,43.75L87.618,43.75z M87.618,22.46H33.215V7.702h54.403V22.46z"></path></svg>
                                </span>
                            </div>
                            <span class="menu-icon">Header Component</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <main class="main-menu">


