<?php include_once('header.php'); ?>
<?php include_once('sidebar.php'); ?>
<div class="Polaris-Page Polaris-Page--fullWidth">
    <div class="Polaris-Page__Content">
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Form</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <div class="Polaris-Layout">
                                    <div class="Polaris-Layout__Section">
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-FormLayout">
                                                    <div role="group" class="Polaris-FormLayout--condensed">
                                                        <div class="Polaris-FormLayout__Items">
                                                            <!--Textfield strat-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Textfield</label></div>
                                                                </div>
                                                                <div class="Polaris-TextField Polaris-TextField--hasValue">
                                                                    <input id="TextField25" type="text" class="Polaris-TextField__Input" aria-labelledby="TextField25Label" aria-invalid="false" value="">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                            </div>
                                                            <!--Textfield end-->
                                                            <!--Textfield with placeholder strat-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label">
                                                                        <label class="Polaris-Label__Text">Textfield with placeholder text</label>
                                                                    </div>
                                                                </div>
                                                                <div class="Polaris-TextField Polaris-TextField--hasValue">
                                                                    <input id="TextField52" type=text placeholder="e.g. North America, Europe" class="Polaris-TextField__Input" aria-labelledby="TextField52Label" aria-invalid="false" value="">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                            </div>
                                                            <!--Textfield with placeholder end-->
                                                            <!--Help Content Textfield start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Help Content Textfield</label></div>
                                                                </div>
                                                                <div class="Polaris-TextField Polaris-TextField--hasValue"><input id="TextField30" class="Polaris-TextField__Input" type="text" aria-describedby="TextField30HelpText" aria-labelledby="TextField30Label" aria-invalid="false" value="">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                                <div class="Polaris-Labelled__HelpText" id="TextField30HelpText">We’ll use this address if we need to contact you about your account.</div>
                                                            </div>
                                                            <!--Help Content Textfield end-->
                                                            <!--Text field with prefix start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Text field with prefix</label></div>
                                                                </div>
                                                                <div class="Polaris-TextField"><input id="" name="" placeholder="Text field with prefix" class="Polaris-TextField__Input" aria-labelledby="TextField3Label" aria-invalid="false">
                                                                    <div class="Polaris-TextField__Prefix mr-3" id="TextField1Prefix">gm</div>
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                            </div>
                                                            <!--Text field with prefix end-->
                                                        </div>
                                                        <div class="Polaris-FormLayout__Items">
                                                            <!--Text field with suffix start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Text field with suffix</label></div>
                                                                </div>

                                                                <div class="Polaris-TextField">
                                                                    <div class="Polaris-TextField__Prefix" id="TextField1Prefix">$</div>
                                                                    <input id="" name="" class="Polaris-TextField__Input" aria-labelledby="TextField3Label" aria-invalid="false">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                            </div>
                                                            <!--Text field with suffix end-->
                                                            <!-- Number field start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Number Field</label></div>
                                                                </div>
                                                                <div class="Polaris-TextField Polaris-TextField--hasValue"><input id="myNumber" name="myNumber" class="Polaris-TextField__Input" type="number" aria-labelledby="TextField32Label" aria-invalid="false" value="">
                                                                    <div class="Polaris-TextField__Spinner" aria-hidden="true">
                                                                        <button role="button" class="Polaris-TextField__Segment down" tabindex="-1">
                                                                            <div class="Polaris-TextField__SpinnerIcon"><span class="Polaris-Icon tip" data-hover="Down"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M15 12l-5-5-5 5z"></path></svg></span></div>
                                                                        </button>
                                                                        <button role="button" class="Polaris-TextField__Segment up" tabindex="-1">
                                                                            <div class="Polaris-TextField__SpinnerIcon"><span class="Polaris-Icon tip" data-hover="Up"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M5 8l5 5 5-5z" fill-rule="evenodd"></path></svg></span></div>
                                                                        </button>
                                                                    </div>
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                            </div>
                                                            <!-- Number field end-->
                                                            <!--Default select start-->
                                                            <div class="Polaris-FormLayout__Item coutnry-select2">
                                                                <div class="Polaris-Labelled__LabelWrapper mb-0">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Select2</label></div>
                                                                </div>
                                                                <div class="Polaris-Select Polaris-Select--placeholder">
                                                                    <select class="Polaris-Select__Input countrySelect2">
                                                                        <option value="india">India</option>
                                                                        <option value="afghanistan">Afghanistan</option>
                                                                        <option value="albania">Albania</option>
                                                                        <option value="Angola">Angola</option>
                                                                        <option value="Anguilla">Anguilla</option>
                                                                        <option value="Aruba">Aruba</option>
                                                                        <option value="Benin">Benin</option>
                                                                    </select>
                                                                    <div class="Polaris-Select__Icon select-hide">
                                                                        <span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path></svg></span></div>
                                                                    <div class="Polaris-Select__Backdrop select-hide"></div>
                                                                </div>
                                                            </div>
                                                            <!--Default select end-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-FormLayout">
                                                    <div role="group" class="Polaris-FormLayout--condensed">
                                                        <div class="Polaris-FormLayout__Items">
                                                            <!--Checkbox start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Checkbox</label></div>
                                                                </div>
                                                                <label class="Polaris-Choice" for="Checkbox18"><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox"><input id="Checkbox18" type="checkbox" class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false" value=""><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></svg></span></span></span></span><span class="Polaris-Choice__Label">Reading</span></label>
                                                            </div>
                                                            <!--Checkbox end-->
                                                            <!--Radio button start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Radio button</label></div>
                                                                </div>
                                                                <div class="Polaris-Stack Polaris-Stack--vertical">
                                                                    <div class="Polaris-Stack__Item">
                                                                        <div><label class="Polaris-Choice" for="disabled"><span class="Polaris-Choice__Control"><span class="Polaris-RadioButton"><input id="disabled" name="accounts" type="radio" class="Polaris-RadioButton__Input" aria-describedby="disabledHelpText" value=""><span class="Polaris-RadioButton__Backdrop"></span><span class="Polaris-RadioButton__Icon"></span></span></span><span class="Polaris-Choice__Label">Accounts are disabled</span></label>
                                                                            <div
                                                                                class="Polaris-Choice__Descriptions">
                                                                                <div class="Polaris-Choice__HelpText" id="disabledHelpText">Customers will only be able to check out as guests.</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="Polaris-Stack__Item">
                                                                        <div><label class="Polaris-Choice" for="optional"><span class="Polaris-Choice__Control"><span class="Polaris-RadioButton"><input id="optional" name="accounts" type="radio" class="Polaris-RadioButton__Input" aria-describedby="optionalHelpText" value=""><span class="Polaris-RadioButton__Backdrop"></span><span class="Polaris-RadioButton__Icon"></span></span></span><span class="Polaris-Choice__Label">Accounts are optional</span></label>
                                                                            <div
                                                                                class="Polaris-Choice__Descriptions">
                                                                                <div class="Polaris-Choice__HelpText" id="optionalHelpText">Customers will be able to check out with a customer account or as a guest.</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--Radio button end-->
                                                            <!--ChoiceList start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">ChoiceList</label></div>
                                                                </div>
                                                                <fieldset class="Polaris-ChoiceList">
                                                                    <!--                                                                    <legend class="Polaris-ChoiceList__Title">Company name</legend>-->
                                                                    <ul class="Polaris-ChoiceList__Choices">
                                                                        <li><label class="Polaris-Choice" for="RadioButton4"><span class="Polaris-Choice__Control"><span class="Polaris-RadioButton"><input id="RadioButton4" name="ChoiceList2" type="radio" class="Polaris-RadioButton__Input" value="hidden"><span class="Polaris-RadioButton__Backdrop"></span><span class="Polaris-RadioButton__Icon"></span></span></span><span class="Polaris-Choice__Label">Hidden</span></label></li>
                                                                        <li><label class="Polaris-Choice" for="RadioButton5"><span class="Polaris-Choice__Control"><span class="Polaris-RadioButton"><input id="RadioButton5" name="ChoiceList2" type="radio" class="Polaris-RadioButton__Input" value="optional"><span class="Polaris-RadioButton__Backdrop"></span><span class="Polaris-RadioButton__Icon"></span></span></span><span class="Polaris-Choice__Label">Optional</span></label></li>
                                                                        <li><label class="Polaris-Choice" for="RadioButton6"><span class="Polaris-Choice__Control"><span class="Polaris-RadioButton"><input id="RadioButton6" name="ChoiceList2" type="radio" class="Polaris-RadioButton__Input" value="required"><span class="Polaris-RadioButton__Backdrop"></span><span class="Polaris-RadioButton__Icon"></span></span></span><span class="Polaris-Choice__Label">Required</span></label></li>
                                                                    </ul>
                                                                </fieldset>
                                                            </div>
                                                            <!--ChoiceList end-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-FormLayout">
                                                    <div role="group" class="Polaris-FormLayout--condensed">
                                                        <div class="Polaris-FormLayout__Items">
                                                            <!--Colorpicker start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Colorpicker</label></div>
                                                                </div>
                                                                <div class="input-group">
                                                                    <input type="text" data-id="icons_background_color" value="#000" name="icon_background_color" class="form-control" aria-describedby="basic-addon1">
                                                                    <span class="my_background_color input-group-btn" ><input type='text' data-id="icons_background_color" class='form-control spectrumColor' value="#000" /></span>
                                                                </div>
                                                            </div>
                                                            <!--Colorpicker end-->
                                                            <!--Transparent colorpicker start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Colorpicker Transparent</label></div>
                                                                </div>
                                                                <div class="input-group">
                                                                    <input type="text" data-id="icons_background_color_transparent" value="#000" name="icon_background_color_transparent" class="form-control" aria-describedby="basic-addon1">
                                                                    <span class="my_background_color_transparent input-group-btn" ><input type='text' data-id="icons_background_color_transparent" class='form-control spectrumTransparentColor' value="#000" /></span>
                                                                </div>
                                                            </div>
                                                            <!--Transparent colorpicker end-->
                                                            <!--Textfield with validation error start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Textfield with validation error</label></div>
                                                                </div>
                                                                <div class="Polaris-TextField Polaris-TextField--hasValue Polaris-TextField--error"><input id="TextField28" type="text" class="Polaris-TextField__Input" aria-describedby="TextField28Error" aria-labelledby="TextField28Label" aria-invalid="true" value="">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                                <div id="TextField28Error" class="Polaris-Labelled__Error">
                                                                    <div class="Polaris-Labelled__ErrorIcon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M10 18a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm-1-8h2V6H9v4zm0 4h2v-2H9v2z" fill-rule="evenodd"></path></svg></span></div>Store name
                                                                    is required</div>
                                                            </div>
                                                            <!--Textfield with validation error end-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Add more item section start-->
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-FormLayout">
                                                    <div role="group" class="Polaris-FormLayout--condensed">
                                                        <div class="Polaris-FormLayout__Items">
                                                            <!--Add more start-->
                                                            <div class="Polaris-FormLayout__Item optionForUnit">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Add More</label></div>
                                                                    <div class="addItemValue"><button type="button" class="Polaris-Button Polaris-Button--primary" data-uno="0" ><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 9h-6V3a1 1 0 1 0-2 0v6H3a1 1 0 1 0 0 2h6v6a1 1 0 1 0 2 0v-6h6a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg></span></span><span>Add unit</span></span></button></div>
                                                                </div>
                                                                <?php
                                                                $template = file_get_contents("template/add-more.html");
                                                                $find_arr = array('UBP:WHICH_SVG_ICON', 'UBP:UNIT_ID', 'UBP:ITEM_CSS');
                                                                $replace_arr = array('<svg class="Polaris-Icon__Svg" viewBox="0 0 55 55" focusable="false" aria-hidden="true"><path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28h-25c-1.104,0-2-0.896-2-2  s0.896-2,2-2h25c1.104,0,2,0.896,2,2S39.604,28,38.5,28z" fill-rule="evenodd" fill="#DE3618"></path></svg>', 0, 'display:none');
                                                                echo str_replace($find_arr, $replace_arr, $template);
                                                                ?>
                                                            </div>
                                                            <!--Add more end-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Add more item section End-->
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-FormLayout">
                                                    <div role="group" class="Polaris-FormLayout--condensed">
                                                        <div class="Polaris-FormLayout__Items">
                                                            <!--Text field with connected fields start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Text field with connected fields</label></div>
                                                                </div>
                                                                <div class="Polaris-Connected">
                                                                    <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                                        <div class="Polaris-TextField Polaris-TextField--hasValue">
                                                                            <input type="text" id="Text-browse-customer" class="Polaris-TextField__Input browse-customer-search" aria-invalid="false" autocomplete="off">
                                                                            <div class="Polaris-TextField__Backdrop"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection">
                                                                        <div class="Polaris-Labelled--hidden">
                                                                            <button type="button" class="Polaris-Button browse-customer" data-toggle="modal" data-target="#add-product-popup"><span class="Polaris-Button__Content"><span>Browse</span></span></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--Text field with connected fields end-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-FormLayout">
                                                    <div role="group" class="Polaris-FormLayout--condensed">
                                                        <div class="Polaris-FormLayout__Items">
                                                            <!--Default select start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Default select</label></div>
                                                                </div>
                                                                <div class="Polaris-Select">
                                                                    <select id="Select13" class="Polaris-Select__Input" aria-invalid="false">
                                                                        <option value="today">Today</option>
                                                                        <option value="yesterday">Yesterday</option>
                                                                        <option value="lastWeek">Last 7 days</option>
                                                                    </select>
                                                                    <div class="Polaris-Select__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path></svg></span></div>
                                                                    <div class="Polaris-Select__Backdrop"></div>
                                                                </div>
                                                            </div>
                                                            <!--Default select end-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-FormLayout">
                                                    <div role="group" class="Polaris-FormLayout--condensed">
                                                        <div class="Polaris-FormLayout__Items">
                                                            <!--Button Datepicker start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Button Datepicker</label></div>
                                                                </div>
                                                                <div class="Polaris-ButtonGroup">
                                                                    <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain"><button type="button" class="Polaris-Button btn-report-daterangepicker" aria-haspopup="true" aria-expanded="true"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M4 8h12V6H4v2zm9 4h2v-2h-2v2zm-4 0h2v-2H9v2zm0 4h2v-2H9v2zm-4-4h2v-2H5v2zm0 4h2v-2H5v2zM17 4h-2V3a1 1 0 1 0-2 0v1H7V3a1 1 0 1 0-2 0v1H3a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z" fill-rule="evenodd"></path></svg></span></span><span id="dateRangeSpan">Today</span></span></button></div>
                                                                </div>
                                                            </div>
                                                            <!--Button Datepicker end-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <!--Input Datepicker start-->
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Input Datepicker</label></div>
                                                                </div>
                                                                <div class="Polaris-Connected">
                                                                    <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                                        <div class="Polaris-TextField Polaris-TextField--hasValue">
                                                                            <input type="text" id="daterange" name="daterange" class="Polaris-TextField__Input browse-customer-search" aria-invalid="false" autocomplete="off">
                                                                            <div class="Polaris-TextField__Backdrop"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection report-btn">
                                                                        <div class="Polaris-Labelled--hidden">
                                                                            <button type="button" class="Polaris-Button  Polaris-Button--sizeSlim report-daterangepicker"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M4 8h12V6H4v2zm9 4h2v-2h-2v2zm-4 0h2v-2H9v2zm0 4h2v-2H9v2zm-4-4h2v-2H5v2zm0 4h2v-2H5v2zM17 4h-2V3a1 1 0 1 0-2 0v1H7V3a1 1 0 1 0-2 0v1H3a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z" fill-rule="evenodd"></path></svg></span></span></span></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--Input Datepicker end-->
                                                            </div>
                                                            <div class="Polaris-FormLayout__Item">
                                                                <!--Input Datepicker start-->
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Input Single Datepicker</label></div>
                                                                </div>
                                                                <div class="Polaris-Connected">
                                                                    <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                                        <div class="Polaris-TextField Polaris-TextField--hasValue">
                                                                            <input type="text" id="single-daterange" name="single-daterange" class="Polaris-TextField__Input browse-customer-search" aria-invalid="false" autocomplete="off">
                                                                            <div class="Polaris-TextField__Backdrop"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="Polaris-Connected__Item Polaris-Connected__Item--connection report-btn">
                                                                        <div class="Polaris-Labelled--hidden">
                                                                            <button type="button" class="Polaris-Button  Polaris-Button--sizeSlim input-single-daterangepicker"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M4 8h12V6H4v2zm9 4h2v-2h-2v2zm-4 0h2v-2H9v2zm0 4h2v-2H9v2zm-4-4h2v-2H5v2zm0 4h2v-2H5v2zM17 4h-2V3a1 1 0 1 0-2 0v1H7V3a1 1 0 1 0-2 0v1H3a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z" fill-rule="evenodd"></path></svg></span></span></span></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--Input Datepicker end-->
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-FormLayout">
                                                    <div role="group" class="Polaris-FormLayout--condensed">
                                                        <div class="Polaris-FormLayout__Items">

                                                            <!-- Tag field start-->
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-Labelled__LabelWrapper">
                                                                    <div class="Polaris-Label"><label class="Polaris-Label__Text">Tag</label></div>
                                                                </div>
                                                                <span class="Polaris-Tag"><span>Wholesale</span><button type="button" aria-label="Remove Wholesale" class="Polaris-Tag__Button"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span></button></span>
                                                            </div>
                                                            <!-- Tag field end-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-Card">
                                            <div class="Polaris-Card__Header">
                                                <h2 class="Polaris-Heading">Upload</h2>
                                            </div>
                                            <div class="Polaris-Card__Section">
                                                <div class="Polaris-Stack Polaris-Stack--vertical">
                                                    <div class="Polaris-Stack__Item">
                                                        <div class="Polaris-DropZone Polaris-DropZone--hasOutline Polaris-DropZone--sizeExtraLarge">
                                                            <div class="Polaris-DropZone__Container">
                                                                <div class="Polaris-DropZone-FileUpload">
                                                                    <div class="Polaris-Stack Polaris-Stack--vertical">
                                                                        <div class="Polaris-Stack__Item" id="image_preview"><img id="blah" class="Polaris-DropZone-FileUpload__Image Polaris-DropZone-FileUpload--sizeExtraLarge" src="data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMTI5IDEwOCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxwYXRoIGQ9Ik02OC43OCAxMDYuMjVhMS41MiAxLjUyIDAgMCAxLTEuMDYtLjQ0IDEuNTQgMS41NCAwIDAgMS0uMzItLjQ5IDEuMzkgMS4zOSAwIDAgMS0uMTItLjU3IDEuNDEgMS40MSAwIDAgMSAuMTItLjU4Ljk0Ljk0IDAgMCAxIC4xNC0uMjYgMSAxIDAgMCAxIC4xOC0uMjIgMS41NCAxLjU0IDAgMCAxIDIuMTIgMCAxLjUgMS41IDAgMCAxLTEuMDYgMi41NnptLTkuNzgtMS41YTEuNSAxLjUgMCAxIDEgMyAwIDEuNSAxLjUgMCAwIDEtMyAwem0tOC4zMSAwYTEuNSAxLjUgMCAwIDEgMyAwIDEuNTEgMS41MSAwIDAgMS0xLjUgMS41IDEuNSAxLjUgMCAwIDEtMS41My0xLjVoLjAzem0tOC4zMiAwYTEuNSAxLjUgMCAxIDEgMS41IDEuNSAxLjUgMS41IDAgMCAxLTEuNTMtMS41aC4wM3ptLTguMzEgMGExLjUgMS41IDAgMSAxIDEuNSAxLjUgMS41IDEuNSAwIDAgMS0xLjU2LTEuNWguMDZ6bS04LjMxIDBhMS41IDEuNSAwIDEgMSAxLjUgMS41IDEuNSAxLjUgMCAwIDEtMS41My0xLjVoLjAzem0tOC4zMiAwYTEuNSAxLjUgMCAxIDEgMS41IDEuNSAxLjUxIDEuNTEgMCAwIDEtMS41My0xLjVoLjAzem0tOC4zMSAwYTEuNSAxLjUgMCAxIDEgMS41IDEuNSAxLjUgMS41IDAgMCAxLTEuNTMtMS41aC4wM3ptLTYuODQgMS41YTEuNSAxLjUgMCAwIDEtMS4wNi0uNDQgMS41IDEuNSAwIDAgMSAwLTIuMTIgMS4wNiAxLjA2IDAgMCAxIC4yMy0uMTkgMS4wOCAxLjA4IDAgMCAxIC4yNi0uMTQgMS4zMSAxLjMxIDAgMCAxIC4yOC0uMDkgMS44OCAxLjg4IDAgMCAxIC41OCAwIDEuMzEgMS4zMSAwIDAgMSAuMjguMDkgMS4zIDEuMyAwIDAgMSAuMjYuMTQgMS4zNyAxLjM3IDAgMCAxIC4yMy4xOSAxLjUgMS41IDAgMCAxIC40NCAxLjA2IDEuNSAxLjUgMCAwIDEtMS41IDEuNXptLTEuNS05LjQzYTEuNSAxLjUgMCAxIDEgMyAwIDEuNSAxLjUgMCAwIDEtMyAwem0wLTcuOTJhMS41IDEuNSAwIDEgMSAzIDAgMS41IDEuNSAwIDAgMS0zIDB6bTAtNy45MmExLjUgMS41IDAgMSAxIDMgLjAyIDEuNSAxLjUgMCAwIDEtMyAwdi0uMDJ6bTAtNy45M2ExLjUgMS41IDAgMSAxIDMgMCAxLjUgMS41IDAgMCAxLTMgMHptMC03LjkyYTEuNSAxLjUgMCAxIDEgMyAwIDEuNSAxLjUgMCAwIDEtMyAwem0wLTcuOTNhMS41IDEuNSAwIDEgMSAzIDAgMS41IDEuNSAwIDAgMS0zIDB6bTAtNy45MmExLjUgMS41IDAgMSAxIDMgMCAxLjUgMS41IDAgMCAxLTMgMHptMS41LTYuNDJhMS41IDEuNSAwIDAgMS0xLjA2LTIuNTYgMS4wNiAxLjA2IDAgMCAxIC4yMy0uMTkgMS41OSAxLjU5IDAgMCAxIC4yNi0uMTFsLjI5LS4xMWExLjQyIDEuNDIgMCAwIDEgLjU4IDBsLjI4LjA4LjI2LjE0YTEuMzcgMS4zNyAwIDAgMSAuMjMuMTkgMS41IDEuNSAwIDAgMS0xLjA2IDIuNTZoLS4wMXpNNTkgNDEuMzZhMS41IDEuNSAwIDEgMSAzIDAgMS41IDEuNSAwIDAgMS0zIDB6bS04LjMxIDBhMS41IDEuNSAwIDEgMSAxLjUgMS41IDEuNSAxLjUgMCAwIDEtMS41My0xLjVoLjAzem0tOC4zMiAwYTEuNSAxLjUgMCAxIDEgMS41IDEuNSAxLjUgMS41IDAgMCAxLTEuNTMtMS41aC4wM3ptLTguMzEgMGExLjUgMS41IDAgMSAxIDEuNSAxLjUgMS41IDEuNSAwIDAgMS0xLjU2LTEuNWguMDZ6bS04LjMxIDBhMS41IDEuNSAwIDEgMSAxLjUgMS41IDEuNSAxLjUgMCAwIDEtMS41My0xLjVoLjAzem0tOC4zMiAwYTEuNSAxLjUgMCAxIDEgMS41IDEuNSAxLjUgMS41IDAgMCAxLTEuNTMtMS41aC4wM3ptLTguMzEgMGExLjUgMS41IDAgMSAxIDEuNSAxLjUgMS41IDEuNSAwIDAgMS0xLjUzLTEuNWguMDN6bTU5LjY2IDEuNWExLjQ5IDEuNDkgMCAwIDEtMS4zOC0yLjA4IDEuMSAxLjEgMCAwIDEgLjE0LS4yNiAxLjI0IDEuMjQgMCAwIDEgLjE4LS4yMiAxLjUyIDEuNTIgMCAwIDEgMS4zNi0uNDFsLjI4LjA4YTIgMiAwIDAgMSAuMjUuMTQgMS4wNiAxLjA2IDAgMCAxIC4yMy4xOSAxLjMgMS4zIDAgMCAxIC4xOS4yMmMwIC4wOS4xLjE3LjE0LjI2YTEuNDcgMS40NyAwIDAgMSAuMDguMjggMS41IDEuNSAwIDAgMS0uNDEgMS4zNiAxIDEgMCAwIDEtLjIzLjE4IDEuMjMgMS4yMyAwIDAgMS0uMjUuMTQgMS40MSAxLjQxIDAgMCAxLS41OC4xMnptLTEuNSA1My45NmExLjUgMS41IDAgMSAxIDMgMCAxLjUgMS41IDAgMCAxLTMgMHptMC03LjkyYTEuNSAxLjUgMCAxIDEgMyAwIDEuNSAxLjUgMCAwIDEtMyAwem0wLTcuOTJjMC0uODMuNjctMS41MDUgMS41LTEuNTFhMS41MSAxLjUxIDAgMCAxIDEuNSAxLjUzIDEuNSAxLjUgMCAwIDEtMyAwdi0uMDJ6bTAtNy45M2ExLjUgMS41IDAgMSAxIDMgMCAxLjUgMS41IDAgMCAxLTMgMHptMC03LjkyYTEuNSAxLjUgMCAxIDEgMyAwIDEuNSAxLjUgMCAwIDEtMyAwem0wLTcuOTNhMS41IDEuNSAwIDEgMSAzIDAgMS41IDEuNSAwIDAgMS0zIDB6bTAtNy45MmExLjUgMS41IDAgMSAxIDMgMCAxLjUgMS41IDAgMCAxLTMgMHoiIGZpbGw9IiNERUU0RjUiIGZpbGwtcnVsZT0ibm9uemVybyIvPjxwYXRoIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE3LjY0IDEuOThoOTEuMjZ2ODcuNTVIMTcuNjR6Ii8+PHBhdGggZD0iTTEwOS43NSA5MC4zOEg5OC4xNmMtMS45MiAwLTMuODgtLjA2LTUuNzMgMC0uOTQgMC0xLjg1LjEtMi44MS4xaC0yLjkxYy0yLS4wNy0zLjk1LS4xNy01Ljg2LS4xNmwtNS42OC4xNS0xMS4zOC4zM2MtMS45MSAwLTMuODEuMTMtNS43NS4xcy0zLjg5LS4xMy01LjgzLS4xN2MtMS45NC0uMDQtMy44NSAwLTUuNzUuMDZzLTMuODEuMTctNS43My4xOWMtMy44Ny0uMDctNy43Mi0uMTQtMTEuNTgtLjI2bC0yLjg4LS4wOGgtOC42M2ExIDEgMCAwIDEtMS0xYzAtMy42NS0uMzktNy4yOS0uMTctMTAuOTRhNDUuNSA0NS41IDAgMCAwIC4yMy01LjQ3Yy0uMDYtLjkxLS4xNy0xLjgzLS4xOC0yLjc0LS4wMS0uOTEgMC0xLjgyIDAtMi43MyAwLTcuMy4yMS0xNC42LjE2LTIxLjg5VjQwLjRjMC0xLjgyLjE3LTMuNjUuMjUtNS40OC4wOC0xLjgzLjE5LTMuNjQuMTMtNS40Ny0uMDYtMS44My0uMjQtMy42NC0uMjMtNS40NyAwLTMuNjUuMy03LjI5LjM3LTEwLjk0LjA3LTMuNjUgMC03LjMtLjA4LTEwLjk1IDAtLjMuMjQtLjU0NS41NC0uNTUgMy44LS4wOSA3LjYxIDAgMTEuNDEgMHM3LjYgMCAxMS40MS4xMWMxLjkgMCAzLjguMDUgNS43IDBsNS43LS4xOGMzLjgxLS4xNiA3LjYxLS4yNCAxMS40MS0uMzMgMy44LS4wOSA3LjYtLjEyIDExLjQxLS4xOCAzLjgxLS4wNiA3LjYuMjEgMTEuNC4yNiAzLjguMDUgNy42MS0uMTYgMTEuNDEtLjI0IDMuOC0uMDggNy42MS0uMzMgMTEuNDEtLjFoLjA4QTEuMTkgMS4xOSAwIDAgMSAxMTAuMTMgMmMwIC45My0uMDYgMS44NS0uMDcgMi43N2wuMDYgMi43Ni4xMSA1LjU0YzAgMS44NSAwIDMuNjktLjEyIDUuNTItLjEyIDEuODMtLjI2IDMuNjQtLjI3IDUuNDggMCAzLjY4LjE2IDcuNDEtLjA5IDExYTQ1LjIxIDQ1LjIxIDAgMCAwIC4xMSA1LjU2IDUzLjcyIDUzLjcyIDAgMCAxIDAgNS41M2MtLjE2IDMuNjItLjI1IDcuMjUtLjI2IDEwLjkzLS4wMSAzLjY4LjE3IDcuNDcgMCAxMS0uMTcgMy41MyAwIDcuMzQuMDggMTEuMTEuMDYgMS45IDAgMy43My4wNiA1LjU5LjA2IDEuODYuMDMgMy43Ny4wMSA1LjU5em0tMS43LTEuN1Y3Ny44N2MuMDYtMy41NS4xOC03LjA4LS4wNi0xMC44NS0uMS0xLjg3LS4xOS0zLjcyLS4xNy01LjUxbC4xMS01LjM0YzAtMy42LS4xOC03LjMtLjM3LTExYTM2Ljg5IDM2Ljg5IDAgMCAxIDAtNS4zNyAzMC41MiAzMC41MiAwIDAgMCAuMTgtNS4zNSA1MCA1MCAwIDAgMS0uMTYtNS40NWMwLTEuNzguMjMtMy41NS4yMy01LjM2di0yLjcybC0uMTYtMi43M2MtLjEyLTEuODItLjEyLTMuNjItLjA5LTUuNDJsLjE3LTUuNC4wOC0yLjdWMS45NWwxLjE0IDEuMTVjLTcuNjEuMzItMTUuMjEtLjM5LTIyLjgyLS4xOC0xLjkgMC0zLjguMTktNS43LjI5LS45NS4wNy0xLjkuMS0yLjg1LjEzLS45NS4wMy0xLjkuMDUtMi44NSAwLTMuODEgMC03LjYxLjA1LTExLjQxLS4wNy0zLjgtLjEyLTcuNi0uMDktMTEuNDEtLjMzYTk0LjMgOTQuMyAwIDAgMC0xMS40LS4yYy03LjYxLjMyLTE1LjIxLjQ4LTIyLjgyLjM4TDE4Ljg0IDJjLS4xNCAzLjY1LS40MSA3LjMtLjI1IDExIC4xNiAzLjcuNTggNy4yOS41OSAxMC45NCAwIDEuODMtLjM0IDMuNjUtLjUgNS40N2EyMC44OSAyMC44OSAwIDAgMCAwIDIuNzRjMCAuOTEuMSAxLjgyLjE4IDIuNzNhNTAuNDEgNTAuNDEgMCAwIDEgLjM2IDUuNDhjLjA1IDEuODItLjA1IDMuNjQtLjA4IDUuNDdsLS4xNCA1LjR2NS40N2wtLjA4IDExdjIuNzNjMCAuOTEtLjE2IDEuODMtLjI0IDIuNzRhNDMuNDUgNDMuNDUgMCAwIDAgLjE0IDUuNDdjLjE3IDMuNjUtLjI0IDcuMjktLjI4IDEwLjk0bC0uODQtLjg0YzcuNTYuMTUgMTUuMTIuMDggMjIuNjcuMDZsNS42Ny4wNmMxLjg5IDAgMy43OSAwIDUuNjUtLjA3bDUuNTktLjE5aDUuNjZsMTEuNC4yIDUuNy4wOWMxLjg5IDAgMy43Mi0uMSA1LjU1LS4xNyAxLjgzLS4wNyAzLjczIDAgNS42NyAwaDUuNjZjMy42My0uMDMgNy40LS4wMiAxMS4xMy0uMDR6IiBmaWxsPSIjREVFNEY1IiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48ZyBvcGFjaXR5PSIuNSIgZmlsbD0iI0RFRTRGNSIgZmlsbC1ydWxlPSJub256ZXJvIj48cGF0aCBkPSJNMjUuOSA5LjloNzQuNzV2NzEuNzFIMjUuOXoiLz48cGF0aCBkPSJNMTAxLjIxIDgyLjE4SDg3LjA2Yy0xLjUyIDAtMy4wOC4xLTQuNjkgMC0xLjYxLS4xLTMuMjEtLjA5LTQuNzctLjA4bC00LjY1LjEtOS4zNC4yMmMtMy4wOS4xMi02LjI5IDAtOS40NSAwLTEuNTgtLjA2LTMuMTQgMC00LjcgMGwtNC43LjExYy02LjMxLS4wNy0xMi42LS4yOS0xOC44Ni0uMjVhLjcuNyAwIDAgMS0uNzEtLjY5YzAtMy0uMjQtNi0uMTEtOWE0NC43MSA0NC43MSAwIDAgMCAuMTYtNC40OGMwLS43NS0uMTItMS41LS4xMi0yLjI0di0yLjE4YzAtNiAuMTQtMTIgLjExLTE3LjkzLS4wMy01LjkzLjU3LTExLjk1LjEtMTcuOTNsLjI0LTljLjA2LTMgMC02LS4wNS05IDAtLjIuMTYtLjM2NS4zNi0uMzcgMy4xMS0uMDYgNi4yMiAwIDkuMzQgMHM2LjIzIDAgOS4zNC4wN2g0LjY3bDQuNjgtLjEyYzMuMTEtLjEgNi4yMy0uMTYgOS4zNC0uMjIgMy4xMS0uMDYgNi4yMy0uMDcgOS4zNC0uMTEgMy4xMS0uMDQgNi4yMy4xMSA5LjM1LjE1IDMuMTIuMDQgNi4yMy0uMDkgOS4zNC0uMTQgMy4xMS0uMDUgNi4yMy0uMTkgOS4zNC0uMDdoLjA2YS44MS44MSAwIDAgMSAuNzcuODJ2NC41MmwuMDYgNC41M2MwIDEuNTEgMCAzLS4wOCA0LjUxLS4wOCAxLjUxLS4xNCAzLS4xNyA0LjQ5IDAgMyAuMSA2IDAgOS0uMDcgMS40OCAwIDMgLjA2IDQuNTMuMDYgMS41MyAwIDMgMCA0LjUzLS4xIDMtLjE2IDUuOTQtLjE3IDguOTUtLjAxIDMuMDEuMTEgNi4wOSAwIDlzMCA2IC4wNSA5LjA4IDAgNi4xNy4wMSA5LjJ6bS0xLjEzLTEuMTN2LTguODhjMC0yLjg4LjEtNS44NSAwLTguOS0uMS0zLjA1LS4wOS02IDAtOC45LjA5LTIuOS0uMTEtNi0uMjQtOWE0MC42MyA0MC42MyAwIDAgMSAwLTQuNDIgMzIgMzIgMCAwIDAgLjEyLTQuNGMtLjI1LTMgMC01LjkxIDAtOC44NyAwLTIuOTYtLjMzLTYtLjE4LTguOTFsLjEtNC40NFY5Ljg4bC43Ny43N2MtMy4xMS4xMi02LjIyIDAtOS4zNCAwcy02LjIzLS4xMi05LjM0LS4wOGMtMy4xMS4wNC02LjIzLjMzLTkuMzUuMy0zLjEyLS4wMy02LjIzIDAtOS4zNCAwLTMuMTEgMC02LjIzLS4wOC05LjM0LS4yM2wtNC42OC0uMTdjLTEuNTUtLjA1LTMuMTEgMC00LjY3IDAtNi4yMy4yMi0xMi40Ni4zMi0xOC42OC4yNWwuOC0uOGMtLjEgMy0uMjggNi0uMTcgOSAuMTEgMyAuMzkgNiAuMzkgOSAwIDEuNS0uMjIgMy0uMzIgNC40OGEzMS44OSAzMS44OSAwIDAgMCAuMSA0LjQ4IDcwLjY5IDcwLjY5IDAgMCAxIC4xOCA5Yy0uMTUgNi0uMDcgMTEuOTUtLjE1IDE3LjkzdjIuMjRjMCAuNzQtLjExIDEuNDktLjE2IDIuMjRhNDEuODcgNDEuODcgMCAwIDAgLjA5IDQuNDhjLjEgMy0uMTUgNi0uMTggOWwtLjU2LS41NmM2LjE5LjEgMTIuMzkgMCAxOC41OCAwIDMuMSAwIDYuMjMuMTEgOS4yOSAwIDMuMDYtLjExIDYuMTItLjIgOS4yNC0uMTFsOS4zNC4xNCA0LjY3LjA2IDQuNTctLjFjMS41MS0uMDYgMy4wNyAwIDQuNjUgMGg0LjY0YzMuMDEtLjE1IDYuMDktLjE0IDkuMTctLjE1eiIvPjwvZz48cmVjdCBmaWxsPSIjREVFNEY1IiBmaWxsLXJ1bGU9Im5vbnplcm8iIHg9IjM0LjY3IiB5PSIyMy4xMSIgd2lkdGg9IjIyLjE1IiBoZWlnaHQ9IjI0LjUyIiByeD0iMTEuMDIiLz48cGF0aCBkPSJNNDUuNzkgNDhhMTIuMDcgMTIuMDcgMCAwIDEtOC42MS0zLjcgMTEuMTEgMTEuMTEgMCAwIDEtMi40Mi00LjE2Yy0uMTItLjM5LS4yLS43OS0uMy0xLjE4LS4xLS4zOS0uMTItLjc5LS4xOC0xLjE5bC0uMDUtMS4yMXYtMS4xNGMwLS43NyAwLTEuNTUuMDctMi4zNiAwLS40LjEzLS44LjE5LTEuMTlsLjMyLTEuMTZhMTEuMDYgMTEuMDYgMCAwIDEgMi40My00LjA4IDExLjM5IDExLjM5IDAgMCAxIDMuODItMi43OCAxMi4yNyAxMi4yNyAwIDAgMSA0LjYyLTEgMTEuMjUgMTEuMjUgMCAwIDEgNC42MSAxIDEyIDEyIDAgMCAxIDMuODggMi42OSAxMS40MSAxMS40MSAwIDAgMSAyLjQzIDQuMDljLjEzLjM3LjIyLjc3LjMyIDEuMTUuMS4zOC4xNC43OS4yMSAxLjE5bC4wNyAxLjE5djEuMTdjMCAuNzcgMCAxLjU1LS4wOSAyLjM1LS4wNS40LS4xMy44LS4xOSAxLjE5TDU2LjY0IDQwYTEwLjg3IDEwLjg3IDAgMCAxLTIuMzUgNC4xNCA3LjQ4IDcuNDggMCAwIDEtLjg2LjgzIDguMjYgOC4yNiAwIDAgMS0uOTQuNzRBMTAuOTIgMTAuOTIgMCAwIDEgNTAuNDQgNDdhMTEuNjIgMTEuNjIgMCAwIDEtNC42NSAxem0wLS43OWExMC4yOSAxMC4yOSAwIDAgMCA3Ljg2LTMuNjQgMTEuMjMgMTEuMjMgMCAwIDAgMi4yMi0zLjgxIDEzIDEzIDAgMCAwIC41My00LjQ2di0yLjI5Yy0uMDYtLjM3LS4wOS0uNzQtLjE2LTEuMTEtLjA3LS4zNy0uMTktLjczLS4zMS0xLjA4YTEwLjY5IDEwLjY5IDAgMCAwLTIuMy0zLjgyIDEwLjQ4IDEwLjQ4IDAgMCAwLTMuNTctMi42NSAxMS4yNyAxMS4yNyAwIDAgMC00LjM3LS43OGgtMS4xMWExMC42IDEwLjYgMCAwIDAtMS4xMS4xNiAxMSAxMSAwIDAgMC0yLjEuNzEgMTAuMjggMTAuMjggMCAwIDAtMy41OCAyLjYxIDE0LjEyIDE0LjEyIDAgMCAwLTIuMjMgMy44MyAxMC4yMiAxMC4yMiAwIDAgMC0uMzEgMS4xMmMwIC4zNy0uMTIuNzQtLjE2IDEuMTEtLjA4Ljc0LS4xMiAxLjUxLS4xNCAyLjI5YTEwLjU3IDEwLjU3IDAgMCAwIDIuODQgOC4zNCAxMC4yMyAxMC4yMyAwIDAgMCA4IDMuNDl2LS4wMnoiIGZpbGw9IiNERUU0RjUiIGZpbGwtcnVsZT0ibm9uemVybyIvPjxwYXRoIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTQ1Ljc0IDM1LjM3aDI4LjkzdjMyLjAySDQ1Ljc0eiIvPjxwYXRoIGQ9Ik03NSA2Ny42N2MtMi40MyAwLTQuOTQtLjA2LTcuMyAwLTIuNTEtLjExLTQuODcgMC03LjI5LjA3LTEuMjEgMC0yLjQxLjA5LTMuNjUgMC0xLjI0LS4wOS0yLjQ2LS4wNy0zLjY2IDAtMi40My4xMi00Ljg5LS4wNy03LjMyLS4wN2EuMzYuMzYgMCAwIDEtLjM2LS4zNnYtNGMwLTEuMzMuMTItMi42NyAwLTQtLjA4LTIuNjYgMC01LjMzIDAtOHYtNGwuMDktMmMwLS42NyAwLTEuMzQtLjA2LTIgMC0xLjMzLjA2LTIuNjYuMDktNCAuMDMtMS4zNCAwLTIuNjcgMC00YS4yLjIgMCAwIDEgLjE5LS4xOWg3LjIzYzIuNDEuMDggNC44My0uMSA3LjI0LS4xMmg3LjI0YzIuNDEuMTEgNC44MiAwIDcuMjMtLjA4YS40Mi40MiAwIDAgMSAuNDMuNGMuMDcgMS4zNS0uMDkgMi42OSAwIDRzLjA3IDIuNzEgMCA0LS4wNyAyLjY3IDAgNGwtLjA3IDItLjAzIDIuMmMwIDEuMzQtLjEgMi42NS0uMTEgNC0uMDEgMS4zNS4wOCAyLjc0IDAgNGwtLjA1IDIgLjA1IDIuMDZDNzUgNjUgNzUgNjYuMzEgNzUgNjcuNjd6bS0uNTctLjU2di02bC0uMDctMmMtLjA5LTEuMzcgMC0yLjYxIDAtNGwtLjE0LTRjMC0uNjYuMTMtMS4yNy4xMi0xLjk0LS4wMS0uNjctLjA2LTEuMzQtLjA4LTJhMzcuMTIgMzcuMTIgMCAwIDAgMC00Yy0uMDktMS4zNCAwLTIuNjQgMC00IDAtMS4zNi0uMDctMi42NSAwLTRsLjQuNGMtMi40MSAwLTQuODItLjE2LTcuMjMgMGwtMS44MS4wN0g2MC4yYy0yLjQxIDAtNC44My0uMjktNy4yNC0uMTMtMi40MS4xNi00LjgyLjE2LTcuMjMuMWwuMzktLjM5YzAgMS4zMy0uMTQgMi42Ny0uMDYgNCAuMDggMS4zMy4yMyAyLjY3LjE2IDRhMTkuNDMgMTkuNDMgMCAwIDAtLjE2IDJjMCAuNjYuMDkgMS4zMy4xNCAyIC4xMiAxLjMzIDAgMi42NiAwIDQtLjA2IDIuNjcgMCA1LjM0LS4xMiA4LS4wOSAxLjM0LjEyIDIuNjcgMCA0cy0uMDcgMi42Ny0uMDkgNGwtLjI2LS4yNWg3LjE5YzEuMiAwIDIuMzkgMCAzLjU3LS4wNyAxLjE4LS4wNyAyLjM4IDAgMy41OCAwIDIuNCAwIDQuODUuMTMgNy4xNyAwIDIuNDUuMjkgNC43Ni4xOSA3LjE1LjIxaC4wNHoiIGZpbGw9IiNGRkYiIGZpbGwtcnVsZT0ibm9uemVybyIvPjxwYXRoIGQ9Ik04MCA0OS40bC05Ljc2IDE5IDIxLjY1LS43LTExLjMxLTE4LjMzYS4zMy4zMyAwIDAgMC0uNTguMDN6IiBmaWxsPSIjREVFNEY1IiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48cGF0aCBkPSJNODAuMzMgNDkuNThjLS44MiAxLjU4LTEuNjIgMy4xNy0yLjQ4IDQuNzNsLTEuMTIgMi40My0xLjIgMi4zOGMtMS42OCAzLjE1LTMuMjkgNi4zMi01IDkuNDVsLS4zMy0uNTJjMS44IDAgMy42MSAwIDUuNDItLjA5czMuNjEgMCA1LjQxLS4xM2MxLjgtLjEzIDMuNjEtLjA5IDUuNDEtLjE2IDEuOC0uMDcgMy42MS0uMjMgNS40MS0uM2wtLjI3LjUxLTMtNC43MWMtMS0xLjU4LTEuODctMy4yLTIuODgtNC43Ni0uNDktLjc5LTEtMS41Ny0xLjQ3LTIuMzdzLS45MS0xLjYyLTEuNDItMi40bC0xLjQ4LTIuMzUtLjc1LTEuMTgtLjM1LS41NC4xLjAxem0tLjctLjM2YS43NC43NCAwIDAgMSAuNTctLjQxLjcxLjcxIDAgMCAxIC42Ni4yN2MuMTkuMjcuMjcuNDEuNC42M2wuNzQgMS4xOCAxLjQ4IDIuMzZjLjQ4LjggMSAxLjU1IDEuNTIgMi4zNC41Mi43OSAxIDEuNTkgMS40NCAyLjM4bDIuODYgNC43OGE2NS4zNSA2NS4zNSAwIDAgMCAxLjQxIDIuNGwxLjUgMi4zNWEuMzguMzggMCAwIDEtLjEyLjU0LjMzLjMzIDAgMCAxLS4xOS4wNmwtMi43MS4xM2MtLjkuMDgtMS44MSAwLTIuNzEgMC0xLjguMDktMy42LjI5LTUuNDEuMzEtMS44MS4wMi0zLjYxLjEzLTUuNDEuMTQtMS44LjAxLTMuNjEuMjYtNS40Mi4xN2EuNDMuNDMgMCAwIDEtLjQxLS40NS40Ni40NiAwIDAgMSAwLS4yYy40NS0uNzcuOS0xLjU0IDEuMjktMi4zNC4zOS0uOC44OC0xLjU1IDEuMy0yLjMzLjg4LTEuNTMgMS41OC0zLjE3IDIuNDEtNC43N0w3NiA1Ni4zNyA3Ny4yNyA1NGMuNzMtMS42IDEuNTctMy4xOSAyLjM2LTQuNzh6IiBmaWxsPSIjREVFNEY1IiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48Y2lyY2xlIGZpbGw9IiNERUU0RjUiIGZpbGwtcnVsZT0ibm9uemVybyIgY3g9IjEwOC42NCIgY3k9Ijg4LjI1IiByPSIxOS41Ii8+PHBhdGggZD0iTTEwOC42NCA3OC42NnYxOS4xOG03LjE4LTkuNTlsLTcuMTgtOS41OS03LjE4IDkuNTkiIHN0cm9rZT0iI0ZGRiIgc3Ryb2tlLXdpZHRoPSIzIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiLz48L2c+PC9zdmc+Cg=="
                                                                                                              alt=""></div>
                                                                        <div class="Polaris-Stack__Item">
                                                                            <button type="button" class="Polaris-Button fileinput-button"  type="file">
                                                                                <span class="Polaris-Button__Content"><span>Add image</span>
                                                                                    <input  type="file" id="upload_file" name="upload_file[]" autocomplete="off" onchange="preview_image();" >
                                                                                </span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="Polaris-Stack__Item"><span class="Polaris-TextStyle--variationSubdued">or drop images to upload</span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <!-- Popup start-->
    <div class="shopify fade modal in" id="add-product-popup" data-backdrop="false" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Demo</h2>
                    <button type="button" class="close shopify-close-btn" data-dismiss="modal" aria-label="Close" class="shopify-close-btn close close-product"><span class="remove-icon Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-4.293 4.293-4.293-4.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l4.293 4.293-4.293 4.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l4.293-4.293 4.293 4.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-4.293-4.293z" fill="#637381" fill-rule="evenodd"></path></svg></span></button>
                </div>
                <div class="modal-body">
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Section">
                            <div class="Polaris-FormLayout">
                                <div role="group" class="">
                                    <div class="Polaris-FormLayout__Item">
                                        <div class="Polaris-TextField">
                                            <div class="Polaris-TextField__Prefix" id=""><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                            <input type="text" id="" name="" class="Polaris-TextField__Input" aria-invalid="false"  placeholder="Search" autocomplete="off">
                                            <div class="Polaris-TextField__Backdrop"></div>
                                        </div>
                                        <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Card__Section Polaris-Card__Section-list">
                            <form method="post" id="product-data">
                                <div class="Polaris-Card">
                                    <div class="Polaris-ResourceList__ResourceListWrapper">
                                        <ul id="ul-track-list" class="Polaris-ResourceList">
                                            <li class="Polaris-ResourceList__ItemWrapper">
                                                <div class="Polaris-ResourceList-Item Polaris-ResourceList-Item--selectable"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" data-polaris-unstyled="true"></a>
                                                    <div class="Polaris-ResourceList-Item__Container">
                                                        <div class="Polaris-ResourceList-Item__Owned">
                                                            <div class="Polaris-ResourceList-Item__Handle">
                                                                <div class="Polaris-ResourceList-Item__CheckboxWrapper"><label class="Polaris-Choice Polaris-Choice--labelHidden" for="chk_5299601436"><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox"><input id="chk_5299601436" name="track_traget_item" value="chk_pages_5299601436" type="checkbox" onclick="item_selected(this.value)" class="track_traget_item Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false"><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></g></svg></span></span></span></span><span class="Polaris-Choice__Label">Select item</span></label></div>
                                                            </div>
                                                        </div>
                                                        <div class="Polaris-ResourceList-Item__Content">
                                                            <h3><span class="Polaris-TextStyle--variationStrong">Akshara In Gorgeous Pink Color Saree</span></h3>
                                                        </div>
                                                    </div>
                                                </div>                                       
                                            </li>
                                            <li class="Polaris-ResourceList__ItemWrapper">
                                                <div class="Polaris-ResourceList-Item Polaris-ResourceList-Item--selectable"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" data-polaris-unstyled="true"></a>
                                                    <div class="Polaris-ResourceList-Item__Container">
                                                        <div class="Polaris-ResourceList-Item__Owned">
                                                            <div class="Polaris-ResourceList-Item__Handle">
                                                                <div class="Polaris-ResourceList-Item__CheckboxWrapper"><label class="Polaris-Choice Polaris-Choice--labelHidden" for="chk_5299601436"><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox"><input id="chk_5299601436" name="track_traget_item" value="chk_pages_5299601436" type="checkbox" onclick="item_selected(this.value)" class="track_traget_item Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false"><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></g></svg></span></span></span></span><span class="Polaris-Choice__Label">Select item</span></label></div>
                                                            </div>
                                                        </div>
                                                        <div class="Polaris-ResourceList-Item__Content">
                                                            <h3><span class="Polaris-TextStyle--variationStrong">Akshara In Gorgeous Pink Color Saree</span></h3>
                                                        </div>
                                                    </div>
                                                </div>                                       
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer pagination">
                    <div class="Polaris-Page__Pagination pagination-popup">
                        <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                            <div id="pagination" class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented"><div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" class="Polaris-Button tip Polaris-Button--disabled" data-hover="Previous"><span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg></span></span></span></a></div><div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" onclick="pagination(2)" class="Polaris-Button tip " data-hover="Next"><span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg></span></span></span></a></div></div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Popup End-->
    <?php include_once('footer.php'); ?>