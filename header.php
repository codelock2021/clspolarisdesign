<?php include_once('config.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title></title>
        <!-- CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/custom_client.css" />
        <link rel="stylesheet" href="assets/css/polaris.css" >
        <link rel="stylesheet" href="assets/css/spectrum.css">
        <link rel="stylesheet" href="assets/css/menu.css">
        <link rel="stylesheet" href="assets/css/select2.min.css" />
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
      
        <link rel="stylesheet" href="assets/css/daterangepicker.css" rel="stylesheet">
        <!-- JS -->
        <script src="assets/js/jquery-2.1.1.js"></script>
        <script src="assets/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="assets/js/custom.js"></script>
        <script src="assets/js/spectrum.js"></script>
        <script src="assets/js/moment.min.js"></script>
        <script src="assets/js/highcharts.js"></script>
        <script src="assets/js/daterangepicker.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/select2.full.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/star_rating.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </head>
    <body>