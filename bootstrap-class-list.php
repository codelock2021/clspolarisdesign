<?php include_once('header.php'); ?>
<?php include_once('sidebar.php'); ?>
<div class="Polaris-Page Polaris-Page--fullWidth">
    <div class="Polaris-Page__Content">
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Bootstrap Class Related List(v4.0.0) <a class="Polaris-Link" href="https://getbootstrap.com/docs/4.1/utilities/text/" target="_BLANK"> (View More Details)</a></h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <ul class="Polaris-List Polaris-List--typeBullet">
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">m</span> - for classes that set <span class="Polaris-TextStyle--variationNegative">margin(m-1)</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">p</span> - for classes that set <span class="Polaris-TextStyle--variationNegative">padding(p-1)</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">t</span> - for classes that set <span class="Polaris-TextStyle--variationNegative">margin-top (mt-4) or padding-top (pt-5)</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">b</span> - for classes that set <span class="Polaris-TextStyle--variationNegative"> margin-bottom (mb-4) or padding-bottom (pb-5)</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">l</span> - for classes that set <span class="Polaris-TextStyle--variationNegative">margin-left (ml-4)  or padding-left (pl-5)</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">r</span> - for classes that set <span class="Polaris-TextStyle--variationNegative">margin-right (mr-4) or padding-right (pr-5)</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">x</span> - for classes that set both <span class="Polaris-TextStyle--variationNegative">*-left and *-right (mx-4) </span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">y</span> - for classes that set both <span class="Polaris-TextStyle--variationNegative">*-top and *-bottom (my-4)</span></li>
                                    <li class="Polaris-List__Item">Blank - for classes that set a <span class="Polaris-TextStyle--variationNegative">margin(m)</span> or <span class="Polaris-TextStyle--variationNegative">padding (p)</span> on all 4 sides of the element</li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">0 - </span>for classes that eliminate the <span class="Polaris-TextStyle--variationNegative">margin(m-0) </span>or <span class="Polaris-TextStyle--variationNegative">padding (p-0)</span> by setting it to <span class="Polaris-TextStyle--variationNegative"> 0 </span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">1 - </span>(by default) for classes that set the <span class="Polaris-TextStyle--variationNegative">margin (m-0)</span>or <span class="Polaris-TextStyle--variationNegative">padding (p-0)</span> to <span class="Polaris-TextStyle--variationNegative"> $spacer * .25</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">2 - </span>(by default) for classes that set the <span class="Polaris-TextStyle--variationNegative">margin (m-1)</span>or <span class="Polaris-TextStyle--variationNegative">padding (p-1)</span> to <span class="Polaris-TextStyle--variationNegative"> $spacer * .5</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">3 - </span>(by default) for classes that set the <span class="Polaris-TextStyle--variationNegative">margin (m-2)</span>or <span class="Polaris-TextStyle--variationNegative">padding (p-2)</span>to <span class="Polaris-TextStyle--variationNegative"> $spacer</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">4 - </span>(by default) for classes that set the <span class="Polaris-TextStyle--variationNegative">margin (m-3)</span>or <span class="Polaris-TextStyle--variationNegative">padding (p-3) </span>to <span class="Polaris-TextStyle--variationNegative"> $spacer * 1.5</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">5 - </span>(by default) for classes that set the <span class="Polaris-TextStyle--variationNegative">margin (m-4)</span>or <span class="Polaris-TextStyle--variationNegative">padding (p-4)</span>to <span class="Polaris-TextStyle--variationNegative"> $spacer * 3</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.text-left</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.text-center</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.text-right</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.text-justify</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.text-lowercase</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.text-uppercase</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.text-capitalize</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.float-left</span></li>
                                    <li class="Polaris-List__Item"><span class="Polaris-TextStyle--variationNegative">.float-right  </span></li>
                                    <li class="Polaris-List__Item">Note: (You can add more sizes by adding entries to the $spacers Sass map variable.)</li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once('footer.php'); ?>