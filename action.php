<?php include_once('header.php'); ?>
<?php include_once('sidebar.php'); ?>
<div class="Polaris-Page">
    <div class="Polaris-Page__Content">
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <!-- popver start-->
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Popver</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <button type="button" class="Polaris-Button" data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" id="action-list" tabindex="0" aria-controls="Popover11" aria-owns="Popover11" aria-haspopup="true" aria-expanded="false">
                                    <span class="Polaris-Button__Content"><span>More actions</span></span>
                                </button>
                                <button type="button" class="Polaris-Button" data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" id="content-action" tabindex="0" aria-controls="Popover11" aria-owns="Popover11" aria-haspopup="true" aria-expanded="false">
                                    <span class="Polaris-Button__Content"><span>Sales channels</span></span>
                                </button>
                                <button type="button" class="Polaris-Button" data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" id="form-components" tabindex="0" aria-haspopup="true" aria-expanded="false">
                                    <span class="Polaris-Button__Content"><span>Filter</span>
                                        <span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M5 8l5 5 5-5z" fill-rule="evenodd"></path></svg></span></span></span>
                                </button>
                                <button type="button" class="Polaris-Button" data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" id="action-list-icons" tabindex="0" aria-haspopup="true" aria-expanded="false">
                                    <span class="Polaris-Button__Content"><span>More actions Icons</span>
                                        <span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M5 8l5 5 5-5z" fill-rule="evenodd"></path></svg></span></span></span>
                                </button>
                            </div>
                        </div>
                        <!-- popver end-->
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Button</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <ul class="Polaris-OptionList">
                                    <li class="Polaris-OptionList__Options">
                                        <ul class="Polaris-OptionList__Options">
                                            <!-- Button start-->
                                            <li class="Polaris-OptionList-Option" tabindex="-1"><button type="button" class="Polaris-Button Polaris-Button--outline"><span class="Polaris-Button__Content"><span>Add product</span></span></button></li></br>
                                            <li class="Polaris-OptionList-Option" tabindex="-1"><button type="button" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Save theme</span></span></button>  <code>(Polaris-Button--primary)</code></li></br>
                                            <li class="Polaris-OptionList-Option" tabindex="-1"><button type="button" class="Polaris-Button Polaris-Button--destructive"><span class="Polaris-Button__Content"><span>Delete theme</span></span></button>  <code>(Polaris-Button--destructive)</code></li></br>
                                            <li class="Polaris-OptionList-Option" tabindex="-1"><button type="button" class="Polaris-Button Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>Save variant</span></span></button>  <code>(Polaris-Button--sizeSlim)</code></li></br>
                                            <li class="Polaris-OptionList-Option" tabindex="-1"><button type="button" class="Polaris-Button Polaris-Button--sizeLarge"><span class="Polaris-Button__Content"><span>Create store</span></span></button>  <code>(Polaris-Button--sizeLarge)</code></li></br>
                                            <li class="Polaris-OptionList-Option" tabindex="-1"><button type="button" class="Polaris-Button Polaris-Button--fullWidth"><span class="Polaris-Button__Content"><span>Add customer</span></span></button>  <code>(Polaris-Button--fullWidth)</code></li></br>
                                            <li class="Polaris-OptionList-Option" tabindex="-1"><button type="button" class="Polaris-Button Polaris-Button--disabled" disabled=""><span class="Polaris-Button__Content"><span>Buy shipping label</span></span></button>  <code>(Polaris-Button--disabled)</code></li></br>
                                            <li class="Polaris-OptionList-Option" tabindex="-1"><button type="button" class="Polaris-Button Polaris-Button--disabled Polaris-Button--loading" disabled="" role="alert" aria-busy="true"><span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><svg viewBox="0 0 20 20" class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall" aria-label="Loading" role="status"><path d="M7.229 1.173a9.25 9.25 0 1 0 11.655 11.412 1.25 1.25 0 1 0-2.4-.698 6.75 6.75 0 1 1-8.506-8.329 1.25 1.25 0 1 0-.75-2.385z"></path></svg></span><span>Save product</span></span></button>  <code>(Polaris-Button--loading)</code></li>
                                            <!-- Button end-->
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Button Group</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!-- button group start-->
                                <div class="Polaris-ButtonGroup">
                                    <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span>Cancel</span></span></button></div>
                                    <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Save</span></span></button></div>
                                </div>
                                <!-- button group end-->
                                <br>
                                <!-- button group segment start-->
                                <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                    <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span>Bold</span></span></button></div>
                                    <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span>Italic</span></span></button></div>
                                    <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span>Underline</span></span></button></div>
                                </div>
                                <!-- button group segment end-->
                                </br>
                                <div class="Polaris-Card__Header">
                                    <h2 class="Polaris-Heading">Page Action</h2>
                                </div>
                                </br>
                                <!-- page action start-->
                                <div class="Polaris-PageActions">
                                    <div class="Polaris-Stack Polaris-Stack--spacingTight Polaris-Stack--distributionEqualSpacing">
                                        <div class="Polaris-Stack__Item">
                                            <div class="Polaris-ButtonGroup">
                                                <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button"><span class="Polaris-Button__Content"><span>Delete</span></span></button></div>
                                            </div>
                                        </div>
                                        <div class="Polaris-Stack__Item"><button type="button" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Save</span></span></button></div>
                                    </div>
                                </div>
                                <!-- page action end-->
                            </div>
                        </div>
                        <!-- Model start-->
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Modal</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <button type="button" class="Polaris-Button" data-toggle="modal" data-target="#simple-popup" tabindex="0"  aria-haspopup="true" aria-expanded="false">
                                    <span class="Polaris-Button__Content"><span>Modal</span></span>
                                </button>
                            </div>
                        </div>
                        <!--Model end-->
                        <!--Tab start-->
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Tab</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <div class="Polaris-Card">
                                    <div>
                                        <ul role="tablist" class="Polaris-Tabs Polaris-Tabs--fillSpace">
                                            <li role="presentation" class="Polaris-Tabs__TabContainer">
                                                <button id="all-customersMeasurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true"  onclick="changeTab(event, 'all_tab')">
                                                    <span class="Polaris-Tabs__Title">All</span>
                                                </button>
                                            </li>
                                            <li role="presentation" class="Polaris-Tabs__TabContainer">
                                                <button id="accepts-marketingMeasurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" onclick="changeTab(event, 'acceps_marketing_tab')">
                                                    <span class="Polaris-Tabs__Title">Accepts marketing</span>
                                                </button>
                                            </li>
                                            <li role="presentation" class="Polaris-Tabs__TabContainer">
                                                <button id="repeat-customersMeasurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" onclick="changeTab(event, 'repeat_customers_tab')">
                                                    <span class="Polaris-Tabs__Title">Repeat customers</span>
                                                </button>
                                            </li>
                                    </div>
                                    </ul>
                                    <div id="all_tab" class="Polaris-Card__Section Polaris-Tabs_tabcontent first">
                                        <div class="Polaris-Card__SectionHeader">
                                            <h3 aria-label="All" class="Polaris-Subheading">All</h3>
                                        </div>
                                        <p>Tab 0 selected</p>
                                    </div>
                                    <div id="acceps_marketing_tab" class="Polaris-Card__Section Polaris-Tabs_tabcontent">
                                        <div class="Polaris-Card__SectionHeader">
                                            <h3 aria-label="All" class="Polaris-Subheading">All</h3>
                                        </div>
                                        <p>Tab 1 selected</p>
                                    </div>
                                    <div id="repeat_customers_tab" class="Polaris-Card__Section Polaris-Tabs_tabcontent">
                                        <div class="Polaris-Card__SectionHeader">
                                            <h3 aria-label="All" class="Polaris-Subheading">All</h3>
                                        </div>
                                        <p>Tab 2 selected</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--tab end-->
                      
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Setting toggle</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                  <!-- Setting toggle start-->
                                <div class="Polaris-Card">
                                    <div class="Polaris-Card__Section">
                                        <div class="Polaris-SettingAction">
                                            <div class="Polaris-SettingAction__Setting">This setting is <b class="Polaris-TextStyle--variationStrong">disabled</b>.</div>
                                            <div class="Polaris-SettingAction__Action"><button type="button" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Enable</span></span></button></div>
                                        </div>
                                    </div>
                                </div>
                                  <!--Setting toggle end-->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Error Block <code>(Polaris-Banner--statusCritical)</code> </h2>

                            </div>
                            <div class="Polaris-Card__Section">
                                <!--Error Block start-->
                                <div class="Polaris-Banner Polaris-Banner--statusCritical Polaris-Banner--withinPage" tabindex="0" role="alert" aria-live="polite" aria-labelledby="Banner9Heading" aria-describedby="Banner9Content">
                                    <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorRedDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M2 10c0-1.846.635-3.543 1.688-4.897l11.209 11.209A7.954 7.954 0 0 1 10 18c-4.411 0-8-3.589-8-8m14.312 4.897L5.103 3.688A7.954 7.954 0 0 1 10 2c4.411 0 8 3.589 8 8a7.952 7.952 0 0 1-1.688 4.897M0 10c0 5.514 4.486 10 10 10s10-4.486 10-10S15.514 0 10 0 0 4.486 0 10"></path></g></svg></span></div>
                                    <div>
                                        <div class="Polaris-Banner__Heading" id="Banner9Heading">
                                            <p class="Polaris-Heading">There are 2 errors with this save data:</p>
                                        </div>
                                        <div class="Polaris-Banner__Content" id="Banner9Content">
                                            <ul class="Polaris-List Polaris-List--typeBullet">
                                                <li class="Polaris-List__Item">Name can't be blank</li>
                                                <li class="Polaris-List__Item">Address can't be blank</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Error Block end-->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Default banners</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!--Default banners start-->
                                <div class="Polaris-Banner Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner16Heading" aria-describedby="Banner16Content">
                                    <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><path fill="currentColor" d="M2 3h11v4h6l-2 4 2 4H8v-4H3"></path><path d="M16.105 11.447L17.381 14H9v-2h4a1 1 0 0 0 1-1V8h3.38l-1.274 2.552a.993.993 0 0 0 0 .895zM2.69 4H12v6H4.027L2.692 4zm15.43 7l1.774-3.553A1 1 0 0 0 19 6h-5V3c0-.554-.447-1-1-1H2.248L1.976.782a1 1 0 1 0-1.953.434l4 18a1.006 1.006 0 0 0 1.193.76 1 1 0 0 0 .76-1.194L4.47 12H7v3a1 1 0 0 0 1 1h11c.346 0 .67-.18.85-.476a.993.993 0 0 0 .044-.972l-1.775-3.553z"></path></g></svg></span></div>
                                    <div>
                                        <div class="Polaris-Banner__Heading" id="Banner16Heading">
                                            <p class="Polaris-Heading">Order archived</p>
                                        </div>
                                        <div class="Polaris-Banner__Content" id="Banner16Content">
                                            <p>This order was archived on March 7, 2017 at 3:12pm EDT.</p>
                                        </div>
                                    </div>
                                </div>
                                <!--Default banners end-->  
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Informational banners</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!--Informational  Banner start-->
                                <div class="Polaris-Banner Polaris-Banner--statusInfo Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner18Heading" aria-describedby="Banner18Content">
                                    <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorTealDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><circle cx="10" cy="10" r="9" fill="currentColor"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m1-5v-3a1 1 0 0 0-1-1H9a1 1 0 1 0 0 2v3a1 1 0 0 0 1 1h1a1 1 0 1 0 0-2m-1-5.9a1.1 1.1 0 1 0 0-2.2 1.1 1.1 0 0 0 0 2.2"></path></g></svg></span></div>
                                    <div>
                                        <div class="Polaris-Banner__Heading" id="Banner18Heading">
                                            <p class="Polaris-Heading">USPS has updated their rates</p>
                                        </div>
                                        <div class="Polaris-Banner__Content" id="Banner18Content">
                                            <p>Make sure you know how these changes affect your store.</p>
                                            <div class="Polaris-Banner__Actions">
                                                <div class="Polaris-ButtonGroup">
                                                    <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--outline"><span class="Polaris-Button__Content"><span>Learn more</span></span></button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Informational  Banner end-->
                            </div>
                        </div>

                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Success banner</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!--Suucess Banner start-->
                                <div class="Polaris-Banner Polaris-Banner--statusSuccess Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner19Heading" aria-describedby="Banner19Content">
                                    <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorGreenDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m2.293-10.707L9 10.586 7.707 9.293a1 1 0 1 0-1.414 1.414l2 2a.997.997 0 0 0 1.414 0l4-4a1 1 0 1 0-1.414-1.414"></path></g></svg></span></div>
                                    <div>
                                        <div class="Polaris-Banner__Heading" id="Banner19Heading">
                                            <p class="Polaris-Heading">Your shipping label is ready to print.</p>
                                        </div>
                                        <div class="Polaris-Banner__Content" id="Banner19Content">
                                            <div class="Polaris-Banner__Actions">
                                                <div class="Polaris-ButtonGroup">
                                                    <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--outline"><span class="Polaris-Button__Content"><span>Print label</span></span></button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Success banner end-->
                            </div>
                        </div>
                        <div class="Polaris-Card">
                            <div class="Polaris-Card__Header">
                                <h2 class="Polaris-Heading">Warning banners</h2>
                            </div>
                            <div class="Polaris-Card__Section">
                                <!--Warning Banner start-->
                                <div class="Polaris-Banner Polaris-Banner--statusWarning Polaris-Banner--withinPage" tabindex="0" role="alert" aria-live="polite" aria-labelledby="Banner20Heading" aria-describedby="Banner20Content">
                                    <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorYellowDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m0-13a1 1 0 0 0-1 1v4a1 1 0 1 0 2 0V6a1 1 0 0 0-1-1m0 8a1 1 0 1 0 0 2 1 1 0 0 0 0-2"></path></g></svg></span></div>
                                    <div>
                                        <div class="Polaris-Banner__Heading" id="Banner20Heading">
                                            <p class="Polaris-Heading">Before you can purchase a shipping label, this change needs to be made:</p>
                                        </div>
                                        <div class="Polaris-Banner__Content" id="Banner20Content">
                                            <ul class="Polaris-List Polaris-List--typeBullet">
                                                <li class="Polaris-List__Item">The name of the city you’re shipping to has characters that aren’t allowed. City name can only include spaces and hyphens.</li>
                                            </ul>
                                            <div class="Polaris-Banner__Actions">
                                                <div class="Polaris-ButtonGroup">
                                                    <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--outline"><span class="Polaris-Button__Content"><span>Edit address</span></span></button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Warning Banner end-->
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <!-- Popup start-->
    <div class="shopify modal fade" id="simple-popup" data-backdrop="false" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Simple</h2>
                    <button type="button" class="close shopify-close-btn" data-dismiss="modal" aria-label="Close" class="shopify-close-btn close close-product"><span class="remove-icon Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-4.293 4.293-4.293-4.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l4.293 4.293-4.293 4.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l4.293-4.293 4.293 4.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-4.293-4.293z" fill="#637381" fill-rule="evenodd"></path></svg></span></button>

                </div>
                <div class="modal-body">
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Header">
                            <h2 class="Polaris-Heading">Simple Popup</h2>
                        </div>
                        <div class="Polaris-Card__Section">
                            <p>View a summary of your online store’s performance.</p>
                        </div>
                    </div>  
                </div>
                <div class="modal-footer">
                    <button type="button" class="Polaris-Button" data-dismiss="modal"><span class="Polaris-Button__Content"><span>Cancel</span></span></button>
                </div>
            </div>
        </div>
    </div>
    <!-- Popup End-->
    <!--    more actions popver start-->
    <div class="Polaris-Popover__Wrapper" style="display: none">
        <div id="popover-content-action-list" tabindex="-1" class="Polaris-Popover__Content">
            <div class="Polaris-Popover__Pane Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true">
                <div class="Polaris-ActionList">
                    <div class="Polaris-ActionList__Section--withoutTitle">
                        <ul class="Polaris-ActionList__Actions">
                            <li>
                                <button class="Polaris-ActionList__Item">
                                    <div class="Polaris-ActionList__Content">Import</div>
                                </button>
                            </li>
                            <li>
                                <button class="Polaris-ActionList__Item">
                                    <div class="Polaris-ActionList__Content">Export</div>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    more actions popver end-->
    <!--    more content actions popver end-->
    <div class="Polaris-Popover__Wrapper" style="display: none">
        <div id="popover-content-content-action" tabindex="-1" class="Polaris-Popover__Content">
            <div class="Polaris-Popover__Pane Polaris-Popover__Pane--fixed">
                <div class="Polaris-Popover__Section">
                    <p>Available sales channels</p>
                </div>
            </div>
            <div class="Polaris-Popover__Pane Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true" polaris="[object Object]">
                <div class="Polaris-ActionList">
                    <div class="Polaris-ActionList__Section--withoutTitle">
                        <ul class="Polaris-ActionList__Actions">
                            <li>
                                <button class="Polaris-ActionList__Item">
                                    <div class="Polaris-ActionList__Content">Online store</div>
                                </button>
                            </li>
                            <li>
                                <button class="Polaris-ActionList__Item">
                                    <div class="Polaris-ActionList__Content">Facebook</div>
                                </button>
                            </li>
                            <li>
                                <button class="Polaris-ActionList__Item">
                                    <div class="Polaris-ActionList__Content">Shopify POS</div>
                                </button>
                            </li>
                        </ul>
                        <span class="input-single-daterangepicker"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    more content actions popver end-->
    <!--    form-components popver start -->
    <div class="Polaris-Popover__Wrapper" style="display: none">
        <div id="popover-content-form-components" tabindex="-1" class="Polaris-Popover__Content">
            <div class="Polaris-Popover__Pane Polaris-Scrollable Polaris-Scrollable--vertical Polaris-Scrollable--hasBottomShadow" data-polaris-scrollable="true" polaris="[object Object]">
                <div class="Polaris-Popover__Section">
                    <div class="Polaris-FormLayout">
                        <div class="Polaris-FormLayout__Item">
                            <div class="">
                                <div class="Polaris-Labelled__LabelWrapper">
                                    <div class="Polaris-Label"><label id="Select7Label" for="Select7" class="Polaris-Label__Text">Show all customers where:</label></div>
                                </div>
                                <div class="Polaris-Select">
                                    <select id="Select7" class="Polaris-Select__Input" aria-invalid="false">
                                        <option value="Tagged with">Tagged with</option>
                                    </select>
                                    <div class="Polaris-Select__Icon">
                                        <span class="Polaris-Icon">
                                            <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                                            <path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path>
                                            </svg>
                                        </span>
                                    </div>
                                    <div class="Polaris-Select__Backdrop"></div>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-FormLayout__Item">
                            <div class="">
                                <div class="Polaris-Labelled__LabelWrapper">
                                    <div class="Polaris-Label"><label id="TextField53Label" for="TextField53" class="Polaris-Label__Text">Tags</label></div>
                                </div>
                                <div class="Polaris-TextField">
                                    <input id="TextField53" class="Polaris-TextField__Input" aria-labelledby="TextField53Label" aria-invalid="false" value="">
                                    <div class="Polaris-TextField__Backdrop"></div>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-FormLayout__Item"><button type="button" class="Polaris-Button Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>Add filter</span></span></button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="Polaris-Popover__Wrapper" style="display: none">
        <div id="popover-content-action-list-icons" tabindex="-1" class="Polaris-Popover__Content">
            <div class="Polaris-Popover__Pane Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true" polaris="[object Object]">
                <div class="Polaris-ActionList">
                    <div>
                        <p class="Polaris-ActionList__Title">File options</p>
                        <ul class="Polaris-ActionList__Actions">
                            <li>
                                <button class="Polaris-ActionList__Item">
                                    <div class="Polaris-ActionList__Content">
                                        <div class="Polaris-ActionList__Image">
                                            <span class="Polaris-Icon">
                                                <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                                                <path d="M13.707 6.707a.997.997 0 0 1-1.414 0L11 5.414V13a1 1 0 1 1-2 0V5.414L7.707 6.707a.999.999 0 1 1-1.414-1.414l3-3a.999.999 0 0 1 1.414 0l3 3a.999.999 0 0 1 0 1.414zM17 18H3a1 1 0 1 1 0-2h14a1 1 0 1 1 0 2z"></path>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="Polaris-ActionList__Text">Import file</div>
                                    </div>
                                </button>
                            </li>
                            <li>
                                <button class="Polaris-ActionList__Item">
                                    <div class="Polaris-ActionList__Content">
                                        <div class="Polaris-ActionList__Image">
                                            <span class="Polaris-Icon">
                                                <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                                                <path d="M9.293 13.707l-3-3a.999.999 0 1 1 1.414-1.414L9 10.586V3a1 1 0 1 1 2 0v7.586l1.293-1.293a.999.999 0 1 1 1.414 1.414l-3 3a.999.999 0 0 1-1.414 0zM17 16a1 1 0 1 1 0 2H3a1 1 0 1 1 0-2h14z"></path>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="Polaris-ActionList__Text">Export file</div>
                                    </div>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once('footer.php'); ?>
