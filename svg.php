<?php include_once('header.php'); ?>
<?php include_once('sidebar.php'); ?>
<div class="Polaris-Page Polaris-Page--fullWidth">
    <div class="Polaris-Page__Content">
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <div class="Polaris-Card">
                            <div class="Polaris-ResourceList__ResourceListWrapper">
                                <ul class="Polaris-ResourceList svg-list" aria-live="polite">
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M15 11h-4v4H9v-4H5V9h4V5h2v4h4v2zm-5-9a8 8 0 1 0 0 16 8 8 0 0 0 0-16z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Cirecleplus</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M15 11h-4v4H9v-4H5V9h4V5h2v4h4v2zm-5-9a8 8 0 1 0 0 16 8 8 0 0 0 0-16z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 9h-6V3a1 1 0 1 0-2 0v6H3a1 1 0 1 0 0 2h6v6a1 1 0 1 0 2 0v-6h6a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Add</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M17 9h-6V3a1 1 0 1 0-2 0v6H3a1 1 0 1 0 0 2h6v6a1 1 0 1 0 2 0v-6h6a1 1 0 1 0 0-2&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M10 18a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm-1-8h2V6H9v4zm0 4h2v-2H9v2z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Alert</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M10 18a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm-1-8h2V6H9v4zm0 4h2v-2H9v2z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M10.707 17.707l5-5a.999.999 0 1 0-1.414-1.414L11 14.586V3a1 1 0 1 0-2 0v11.586l-3.293-3.293a.999.999 0 1 0-1.414 1.414l5 5a.999.999 0 0 0 1.414 0" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ArrowDown</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M10.707 17.707l5-5a.999.999 0 1 0-1.414-1.414L11 14.586V3a1 1 0 1 0-2 0v11.586l-3.293-3.293a.999.999 0 1 0-1.414 1.414l5 5a.999.999 0 0 0 1.414 0&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ArrowLeft</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ArrowRight</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11 17V5.414l3.293 3.293a.999.999 0 1 0 1.414-1.414l-5-5a.999.999 0 0 0-1.414 0l-5 5a.997.997 0 0 0 0 1.414.999.999 0 0 0 1.414 0L9 5.414V17a1 1 0 1 0 2 0" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ArrowUp</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M11 17V5.414l3.293 3.293a.999.999 0 1 0 1.414-1.414l-5-5a.999.999 0 0 0-1.414 0l-5 5a.997.997 0 0 0 0 1.414.999.999 0 0 0 1.414 0L9 5.414V17a1 1 0 1 0 2 0&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ArrowUpDown</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M4 8h12V6H4v2zm9 4h2v-2h-2v2zm-4 0h2v-2H9v2zm0 4h2v-2H9v2zm-4-4h2v-2H5v2zm0 4h2v-2H5v2zM17 4h-2V3a1 1 0 1 0-2 0v1H7V3a1 1 0 1 0-2 0v1H3a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Calendar</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M4 8h12V6H4v2zm9 4h2v-2h-2v2zm-4 0h2v-2H9v2zm0 4h2v-2H9v2zm-4-4h2v-2H5v2zm0 4h2v-2H5v2zM17 4h-2V3a1 1 0 1 0-2 0v1H7V3a1 1 0 1 0-2 0v1H3a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Cancel</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M5 8l5 5 5-5z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">CancelSmall</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M5 8l5 5 5-5z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M15 11h-4v4H9v-4H5V9h4V5h2v4h4v2zm-5-9a8 8 0 1 0 0 16 8 8 0 0 0 0-16z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">CaretDown</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M15 11h-4v4H9v-4H5V9h4V5h2v4h4v2zm-5-9a8 8 0 1 0 0 16 8 8 0 0 0 0-16z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M15 12l-5-5-5 5z"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">CaretUp</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M15 12l-5-5-5 5z&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Checkmark</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M10 14a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 1 1 1.414-1.414L10 11.586l4.293-4.293a.999.999 0 1 1 1.414 1.414l-5 5A.997.997 0 0 1 10 14" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ChevronDown</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M10 14a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 1 1 1.414-1.414L10 11.586l4.293-4.293a.999.999 0 1 1 1.414 1.414l-5 5A.997.997 0 0 1 10 14&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ChevronLeft</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ChevronRight</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M15 13a.997.997 0 0 1-.707-.293L10 8.414l-4.293 4.293a.999.999 0 1 1-1.414-1.414l5-5a.999.999 0 0 1 1.414 0l5 5A.999.999 0 0 1 15 13" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">ChevronUp</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M15 13a.997.997 0 0 1-.707-.293L10 8.414l-4.293 4.293a.999.999 0 1 1-1.414-1.414l5-5a.999.999 0 0 1 1.414 0l5 5A.999.999 0 0 1 15 13&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M14.242 12.829l-1.414 1.414L10 11.413l-2.828 2.83-1.414-1.414 2.828-2.83-2.828-2.827 1.414-1.414L10 8.586l2.828-2.828 1.414 1.414L11.414 10l2.828 2.829zM10 1.999A8 8 0 1 0 10 18a8 8 0 0 0 0-16z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">CircleCancel</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M14.242 12.829l-1.414 1.414L10 11.413l-2.828 2.83-1.414-1.414 2.828-2.83-2.828-2.827 1.414-1.414L10 8.586l2.828-2.828 1.414 1.414L11.414 10l2.828 2.829zM10 1.999A8 8 0 1 0 10 18a8 8 0 0 0 0-16z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M10 13.414L5.293 8.707l1.414-1.414L10 10.586l3.293-3.293 1.414 1.414L10 13.414zM10 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">CircleChevronDown</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M10 13.414L5.293 8.707l1.414-1.414L10 10.586l3.293-3.293 1.414 1.414L10 13.414zM10 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M11.293 5.293l1.414 1.414L9.414 10l3.293 3.293-1.414 1.414L6.586 10l4.707-4.707zM10 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">CircleChevronLeft</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M11.293 5.293l1.414 1.414L9.414 10l3.293 3.293-1.414 1.414L6.586 10l4.707-4.707zM10 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M8.707 14.707l-1.414-1.414L10.586 10 7.293 6.707l1.414-1.414L13.414 10l-4.707 4.707zM10 18a8 8 0 1 0 0-16 8 8 0 0 0 0 16z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">CircleChevronRight</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M8.707 14.707l-1.414-1.414L10.586 10 7.293 6.707l1.414-1.414L13.414 10l-4.707 4.707zM10 18a8 8 0 1 0 0-16 8 8 0 0 0 0 16z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li><li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M14.707 11.293l-1.414 1.414L10 9.414l-3.293 3.293-1.414-1.414L10 6.586l4.707 4.707zM18 10a8 8 0 1 0-16 0 8 8 0 0 0 16 0z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">CircleChevronUp</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M14.707 11.293l-1.414 1.414L10 9.414l-3.293 3.293-1.414-1.414L10 6.586l4.707 4.707zM18 10a8 8 0 1 0-16 0 8 8 0 0 0 16 0z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M13 11h2V9h-2v2zm-4 0h2V9H9v2zm-4 0h2V9H5v2zm5-9c-4.411 0-8 3.589-8 8 0 1.504.425 2.908 1.15 4.111l-1.069 2.495a1 1 0 0 0 1.314 1.313l2.494-1.069A7.939 7.939 0 0 0 10 18c4.411 0 8-3.589 8-8s-3.589-8-8-8z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Conversation</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M13 11h2V9h-2v2zm-4 0h2V9H9v2zm-4 0h2V9H5v2zm5-9c-4.411 0-8 3.589-8 8 0 1.504.425 2.908 1.15 4.111l-1.069 2.495a1 1 0 0 0 1.314 1.313l2.494-1.069A7.939 7.939 0 0 0 10 18c4.411 0 8-3.589 8-8s-3.589-8-8-8z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M16 6H4a1 1 0 1 0 0 2h1v9a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V8h1a1 1 0 1 0 0-2zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Delete</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M16 6H4a1 1 0 1 0 0 2h1v9a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V8h1a1 1 0 1 0 0-2zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M10 16a5.961 5.961 0 0 1-3.471-1.115l8.356-8.356A5.961 5.961 0 0 1 16 10c0 3.309-2.691 6-6 6m0-12c1.294 0 2.49.416 3.471 1.115l-8.356 8.356A5.961 5.961 0 0 1 4 10c0-3.309 2.691-6 6-6m0-2c-4.411 0-8 3.589-8 8s3.589 8 8 8 8-3.589 8-8-3.589-8-8-8" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Disable</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M10 16a5.961 5.961 0 0 1-3.471-1.115l8.356-8.356A5.961 5.961 0 0 1 16 10c0 3.309-2.691 6-6 6m0-12c1.294 0 2.49.416 3.471 1.115l-8.356 8.356A5.961 5.961 0 0 1 4 10c0-3.309 2.691-6 6-6m0-2c-4.411 0-8 3.589-8 8s3.589 8 8 8 8-3.589 8-8-3.589-8-8-8&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M9 10h2V6H9v4zm0 4h2v-2H9v2zm-7-4c0 4.411 3.589 8 8 8a7.939 7.939 0 0 0 4.111-1.15l2.494 1.069a1 1 0 0 0 1.314-1.313l-1.069-2.495A7.939 7.939 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Dispute</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M9 10h2V6H9v4zm0 4h2v-2H9v2zm-7-4c0 4.411 3.589 8 8 8a7.939 7.939 0 0 0 4.111-1.15l2.494 1.069a1 1 0 0 0 1.314-1.313l-1.069-2.495A7.939 7.939 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M8 12h8V4H8v8zm4 4H4V8h2v5a1 1 0 0 0 1 1h5v2zm5-14H7a1 1 0 0 0-1 1v3H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1v-3h3a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Duplicate</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M8 12h8V4H8v8zm4 4H4V8h2v5a1 1 0 0 0 1 1h5v2zm5-14H7a1 1 0 0 0-1 1v3H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1v-3h3a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 13a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-3a1 1 0 1 1 2 0v2h12v-2a1 1 0 0 1 1-1zm0-11a1 1 0 0 1 1 1v3a1 1 0 1 1-2 0V4H4v2a1 1 0 1 1-2 0V3a1 1 0 0 1 1-1h14zm.555 7.168a1.001 1.001 0 0 1 0 1.664l-3 2a1 1 0 0 1-1.109-1.664L15.198 10l-1.752-1.168a1 1 0 1 1 1.109-1.664l3 2zM6.832 7.445a1 1 0 0 1-.277 1.387L4.803 10l1.752 1.168a1 1 0 1 1-1.11 1.664l-3-2a1.001 1.001 0 0 1 0-1.664l3-2a1 1 0 0 1 1.387.277zM9 14.001a1 1 0 0 1-.948-1.317l2-6a1 1 0 0 1 1.896.633l-2 6A.999.999 0 0 1 9 14z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Embed</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M17 13a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-3a1 1 0 1 1 2 0v2h12v-2a1 1 0 0 1 1-1zm0-11a1 1 0 0 1 1 1v3a1 1 0 1 1-2 0V4H4v2a1 1 0 1 1-2 0V3a1 1 0 0 1 1-1h14zm.555 7.168a1.001 1.001 0 0 1 0 1.664l-3 2a1 1 0 0 1-1.109-1.664L15.198 10l-1.752-1.168a1 1 0 1 1 1.109-1.664l3 2zM6.832 7.445a1 1 0 0 1-.277 1.387L4.803 10l1.752 1.168a1 1 0 1 1-1.11 1.664l-3-2a1.001 1.001 0 0 1 0-1.664l3-2a1 1 0 0 1 1.387.277zM9 14.001a1 1 0 0 1-.948-1.317l2-6a1 1 0 0 1 1.896.633l-2 6A.999.999 0 0 1 9 14z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M9.293 13.707l-3-3a.999.999 0 1 1 1.414-1.414L9 10.586V3a1 1 0 1 1 2 0v7.586l1.293-1.293a.999.999 0 1 1 1.414 1.414l-3 3a.999.999 0 0 1-1.414 0zM17 16a1 1 0 1 1 0 2H3a1 1 0 1 1 0-2h14z"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Export</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M9.293 13.707l-3-3a.999.999 0 1 1 1.414-1.414L9 10.586V3a1 1 0 1 1 2 0v7.586l1.293-1.293a.999.999 0 1 1 1.414 1.414l-3 3a.999.999 0 0 1-1.414 0zM17 16a1 1 0 1 1 0 2H3a1 1 0 1 1 0-2h14z&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 2a1 1 0 0 1 1 1v4a1 1 0 1 1-2 0V5.414l-7.293 7.293a.997.997 0 0 1-1.414 0 .999.999 0 0 1 0-1.414L14.586 4H13a1 1 0 1 1 0-2h4zm-4 9a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h5a1 1 0 1 1 0 2H4v8h8v-4a1 1 0 0 1 1-1z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">External</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M17 2a1 1 0 0 1 1 1v4a1 1 0 1 1-2 0V5.414l-7.293 7.293a.997.997 0 0 1-1.414 0 .999.999 0 0 1 0-1.414L14.586 4H13a1 1 0 1 1 0-2h4zm-4 9a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h5a1 1 0 1 1 0 2H4v8h8v-4a1 1 0 0 1 1-1z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><circle cx="10" cy="10" r="9" fill="#fff"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m0-4a1 1 0 1 0 0 2 1 1 0 1 0 0-2m0-10C8.346 4 7 5.346 7 7a1 1 0 1 0 2 0 1.001 1.001 0 1 1 1.591.808C9.58 8.548 9 9.616 9 10.737V11a1 1 0 1 0 2 0v-.263c0-.653.484-1.105.773-1.317A3.013 3.013 0 0 0 13 7c0-1.654-1.346-3-3-3"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Help</span></h3>
                                                    <div><code>&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;circle cx=&quot;10&quot; cy=&quot;10&quot; r=&quot;9&quot; fill=&quot;#fff&quot;&gt;&lt;/circle&gt;&lt;path d=&quot;M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m0-4a1 1 0 1 0 0 2 1 1 0 1 0 0-2m0-10C8.346 4 7 5.346 7 7a1 1 0 1 0 2 0 1.001 1.001 0 1 1 1.591.808C9.58 8.548 9 9.616 9 10.737V11a1 1 0 1 0 2 0v-.263c0-.653.484-1.105.773-1.317A3.013 3.013 0 0 0 13 7c0-1.654-1.346-3-3-3&quot;&gt;&lt;/path&gt;&lt;/svg&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M6 10a2 2 0 1 1-4.001-.001A2 2 0 0 1 6 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 12 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 18 10z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">HorizontalDots</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M6 10a2 2 0 1 1-4.001-.001A2 2 0 0 1 6 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 12 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 18 10z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M13.707 6.707a.997.997 0 0 1-1.414 0L11 5.414V13a1 1 0 1 1-2 0V5.414L7.707 6.707a.999.999 0 1 1-1.414-1.414l3-3a.999.999 0 0 1 1.414 0l3 3a.999.999 0 0 1 0 1.414zM17 18H3a1 1 0 1 1 0-2h14a1 1 0 1 1 0 2z"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Import</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M13.707 6.707a.997.997 0 0 1-1.414 0L11 5.414V13a1 1 0 1 1-2 0V5.414L7.707 6.707a.999.999 0 1 1-1.414-1.414l3-3a.999.999 0 0 1 1.414 0l3 3a.999.999 0 0 1 0 1.414zM17 18H3a1 1 0 1 1 0-2h14a1 1 0 1 1 0 2z&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M6 11h8V9H6v2zm0 4h8v-2H6v2zm0-8h4V5H6v2zm9.707-1.707l-3-3A.996.996 0 0 0 12 2H5a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V6a.997.997 0 0 0-.293-.707z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Notes</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M6 11h8V9H6v2zm0 4h8v-2H6v2zm0-8h4V5H6v2zm9.707-1.707l-3-3A.996.996 0 0 0 12 2H5a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V6a.997.997 0 0 0-.293-.707z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M16 8c0-2.967-2.167-5.432-5-5.91V1a1 1 0 1 0-2 0v1.09C6.167 2.568 4 5.033 4 8c0 2.957 0 4.586-1.707 6.293A1 1 0 0 0 3 16h4.183A2.909 2.909 0 0 0 7 17c0 1.654 1.345 3 3 3s3-1.346 3-3c0-.353-.07-.687-.184-1H17a1 1 0 0 0 .707-1.707C16 12.586 16 10.957 16 8zM5.011 14C6 12.208 6 10.285 6 8c0-2.206 1.794-4 4-4s4 1.794 4 4c0 2.285 0 4.208.989 6H5.011zM11 17a1.001 1.001 0 0 1-2 0 1 1 0 0 1 2 0z"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Notification</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M16 8c0-2.967-2.167-5.432-5-5.91V1a1 1 0 1 0-2 0v1.09C6.167 2.568 4 5.033 4 8c0 2.957 0 4.586-1.707 6.293A1 1 0 0 0 3 16h4.183A2.909 2.909 0 0 0 7 17c0 1.654 1.345 3 3 3s3-1.346 3-3c0-.353-.07-.687-.184-1H17a1 1 0 0 0 .707-1.707C16 12.586 16 10.957 16 8zM5.011 14C6 12.208 6 10.285 6 8c0-2.206 1.794-4 4-4s4 1.794 4 4c0 2.285 0 4.208.989 6H5.011zM11 17a1.001 1.001 0 0 1-2 0 1 1 0 0 1 2 0z&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M14 11h2V9h-2v2zM7 7h6V4H7v3zm0 9h6v-2H7v2zm10-9h-2V3a1 1 0 0 0-1-1H6a1 1 0 0 0-1 1v4H3a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h2v1a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1v-1h2a1 1 0 0 0 1-1V8a1 1 0 0 0-1-1z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Print</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M14 11h2V9h-2v2zM7 7h6V4H7v3zm0 9h6v-2H7v2zm10-9h-2V3a1 1 0 0 0-1-1H6a1 1 0 0 0-1 1v4H3a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h2v1a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1v-1h2a1 1 0 0 0 1-1V8a1 1 0 0 0-1-1z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 11a1 1 0 0 1 1 1c0 1.654-1.346 3-3 3H5.414l1.293 1.293a.999.999 0 1 1-1.414 1.414l-3-3a.999.999 0 0 1 0-1.414l3-3a.999.999 0 1 1 1.414 1.414L5.414 13H15c.552 0 1-.449 1-1a1 1 0 0 1 1-1zM3 9a1 1 0 0 1-1-1c0-1.654 1.346-3 3-3h9.586l-1.293-1.293a.999.999 0 1 1 1.414-1.414l3 3a.999.999 0 0 1 0 1.414l-3 3a.997.997 0 0 1-1.414 0 .999.999 0 0 1 0-1.414L14.586 7H5c-.552 0-1 .449-1 1a1 1 0 0 1-1 1z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Refresh</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M17 11a1 1 0 0 1 1 1c0 1.654-1.346 3-3 3H5.414l1.293 1.293a.999.999 0 1 1-1.414 1.414l-3-3a.999.999 0 0 1 0-1.414l3-3a.999.999 0 1 1 1.414 1.414L5.414 13H15c.552 0 1-.449 1-1a1 1 0 0 1 1-1zM3 9a1 1 0 0 1-1-1c0-1.654 1.346-3 3-3h9.586l-1.293-1.293a.999.999 0 1 1 1.414-1.414l3 3a.999.999 0 0 1 0 1.414l-3 3a.997.997 0 0 1-1.414 0 .999.999 0 0 1 0-1.414L14.586 7H5c-.552 0-1 .449-1 1a1 1 0 0 1-1 1z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M9 12h2V8H9v4zm0 4h2v-2H9v2zm8.895.509l-7-14c-.339-.678-1.451-.678-1.79 0l-7 14A.999.999 0 0 0 3 17.956h14a1.001 1.001 0 0 0 .895-1.447z" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Risk</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M9 12h2V8H9v4zm0 4h2v-2H9v2zm8.895.509l-7-14c-.339-.678-1.451-.678-1.79 0l-7 14A.999.999 0 0 0 3 17.956h14a1.001 1.001 0 0 0 .895-1.447z&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 4h-3a1 1 0 1 0 0 2h2v10H4V4h3.586L9 5.414v5.172L7.707 9.293a1 1 0 0 0-1.414 1.414l3 3a.996.996 0 0 0 1.414 0l3-3a1 1 0 0 0-1.414-1.414L11 10.586V5a.997.997 0 0 0-.293-.707l-2-2A.994.994 0 0 0 8 2H3a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Save</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M17 4h-3a1 1 0 1 0 0 2h2v10H4V4h3.586L9 5.414v5.172L7.707 9.293a1 1 0 0 0-1.414 1.414l3 3a.996.996 0 0 0 1.414 0l3-3a1 1 0 0 0-1.414-1.414L11 10.586V5a.997.997 0 0 0-.293-.707l-2-2A.994.994 0 0 0 8 2H3a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Search</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M8 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8m9.707 4.293l-4.82-4.82A5.968 5.968 0 0 0 14 8 6 6 0 0 0 2 8a6 6 0 0 0 6 6 5.968 5.968 0 0 0 3.473-1.113l4.82 4.82a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M15 9H5a1 1 0 1 0 0 2h10a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">Subtract</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M15 9H5a1 1 0 1 0 0 2h10a1 1 0 1 0 0-2&quot; fill-rule=&quot;evenodd&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="Polaris-ResourceList__ItemWrapper">
                                        <div class="Polaris-ResourceList-Item"><a aria-describedby="341" aria-label="View details for Mae Jemison" class="Polaris-ResourceList-Item__Link" href="customers/341" data-polaris-unstyled="true"></a>
                                            <div class="Polaris-ResourceList-Item__Container" id="341">
                                                <div class="Polaris-ResourceList-Item__Owned">
                                                    <div class="Polaris-ResourceList-Item__Media"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17.928 9.628C17.836 9.399 15.611 4 9.999 4S2.162 9.399 2.07 9.628a1.017 1.017 0 0 0 0 .744C2.162 10.601 4.387 16 9.999 16s7.837-5.399 7.929-5.628a1.017 1.017 0 0 0 0-.744zM9.999 14a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-6A2 2 0 1 0 10 12.001 2 2 0 0 0 10 8z"></path></svg></span></div>
                                                </div>
                                                <div class="Polaris-ResourceList-Item__Content">
                                                    <h3><span class="Polaris-TextStyle--variationStrong">View</span></h3>
                                                    <div><code>&lt;span class=&quot;Polaris-Icon&quot;&gt;&lt;svg class=&quot;Polaris-Icon__Svg&quot; viewBox=&quot;0 0 20 20&quot; focusable=&quot;false&quot; aria-hidden=&quot;true&quot;&gt;&lt;path d=&quot;M17.928 9.628C17.836 9.399 15.611 4 9.999 4S2.162 9.399 2.07 9.628a1.017 1.017 0 0 0 0 .744C2.162 10.601 4.387 16 9.999 16s7.837-5.399 7.929-5.628a1.017 1.017 0 0 0 0-.744zM9.999 14a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-6A2 2 0 1 0 10 12.001 2 2 0 0 0 10 8z&quot;&gt;&lt;/path&gt;&lt;/svg&gt;&lt;/span&gt;</code></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                
    <?php include_once('footer.php'); ?>