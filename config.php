<?php

/**
 * Configuration for: Database Connection
 * This is the place where your database login constants are saved
 *
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 * If you want to know why we use "define" instead of "const" @see http://stackoverflow.com/q/2447791/1114320
 *
 * DB_HOST: database host, usually it's "127.0.0.1" or "localhost", some servers also need port info
 * DB_NAME: name of the database. please note: database and database table are not the same thing
 * DB_USER: user for your database. the user needs to have rights for SELECT, UPDATE, DELETE and INSERT.
 *          by the way, it's bad style to use "root", but for development it will work.
 * DB_PASS: the password of the above user
 */
/* define site url and admin details */
define('BASE_URL', 'https://www.appsonrent.com/demo-app');
define('SITE_URL', 'https://www.appsonrent.com/demo-app/');
define('SITE_CLIENT_URL', 'https://www.appsonrent.com/demo-app/client/');

define('SITE_NAME', 'Demo App');
define('SITE_EMAIL', '#');
define('APP_STORE_URL', '#');
define('SITE_COPYRIGHT', SITE_NAME . ' &copy; ' . date('Y') . ' - Made with <span class="copyrightheart">&#10084;</span> by the <a class="Polaris-Link" target="_blank" href="https://www.appsonrent.com">appsorent.com</a> team');
/* * ****************************************
 *      SVG ICON CONSTANT BLOCK           *
 * **************************************** */
define('SVG_EYE', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg>');
define('SVG_CLOSE_EYE', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M10 12a2 2 0 0 0 2-2c0-.178-.03-.348-.074-.512l5.78-5.78a1 1 0 1 0-1.413-1.415l-2.61 2.61A7.757 7.757 0 0 0 10 4C4.388 4 2.163 9.4 2.07 9.628a1.017 1.017 0 0 0 0 .744c.055.133.836 2.01 2.583 3.56l-2.36 2.36a1 1 0 1 0 1.414 1.415l5.78-5.78c.165.042.335.073.513.073zm-4-2a4 4 0 0 1 4-4c.742 0 1.432.208 2.025.56l-1.513 1.514A2.004 2.004 0 0 0 10 8a2 2 0 0 0-2 2c0 .178.03.347.074.51L6.56 12.026A3.96 3.96 0 0 1 6 10zm10.144-3.144l-2.252 2.252c.065.288.107.585.107.893a4 4 0 0 1-4 4c-.308 0-.604-.04-.892-.107l-1.682 1.68a7.903 7.903 0 0 0 2.573.428c5.612 0 7.836-5.398 7.928-5.628a1.004 1.004 0 0 0 0-.743c-.044-.112-.596-1.438-1.784-2.774z" fill="#000" fill-rule="evenodd"></path></svg>');
define('SVG_EDIT', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg>');
define('SVG_DELETE', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>');

define('SVG_NEXT_PAGE', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414" fill-rule="evenodd"></path></svg>');
define('SVG_PREV_PAGE', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg>');

define('SVG_RESET', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path style="fill:#030104;" d="M18,10.473c0-1.948-0.618-3.397-2.066-4.844c-0.391-0.39-1.023-0.39-1.414,0c-0.391,0.391-0.391,1.024,0,1.415C15.599,8.122,16,9.051,16,10.473c0,1.469-0.572,2.85-1.611,3.888c-1.004,1.003-2.078,1.502-3.428,1.593l1.246-1.247c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L7.086,17l3.707,3.707C10.988,20.902,11.244,21,11.5,21s0.512-0.098,0.707-0.293c0.391-0.391,0.391-1.023,0-1.414l-1.337-1.336c1.923-0.082,3.542-0.792,4.933-2.181C17.22,14.36,18,12.477,18,10.473z"/><path style="fill:#030104;" d="M5,10.5c0-1.469,0.572-2.85,1.611-3.889c1.009-1.009,2.092-1.508,3.457-1.594L8.793,6.292c-0.391,0.391-0.391,1.023,0,1.414C8.988,7.902,9.244,8,9.5,8s0.512-0.098,0.707-0.293L13.914,4l-3.707-3.707c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l1.311,1.311C8.19,3.104,6.579,3.814,5.197,5.197C3.78,6.613,3,8.496,3,10.5c0,1.948,0.618,3.397,2.066,4.844c0.195,0.195,0.451,0.292,0.707,0.292s0.512-0.098,0.707-0.293c0.391-0.391,0.391-1.024,0-1.415C5.401,12.851,5,11.922,5,10.5z"></path></svg>');
define('SVG_SEARCH', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg>');

define('SVG_CIRCLE_PLUS', '<svg class="Polaris-Icon__Svg" viewBox="0 0 510 510" focusable="false" aria-hidden="true"><path d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z M382.5,280.5h-102v102h-51v-102    h-102v-51h102v-102h51v102h102V280.5z" fill-rule="evenodd" fill="#3f4eae"></path></svg>');
define('SVG_CIRCLE_MINUS', '<svg class="Polaris-Icon__Svg" viewBox="0 0 80 80" focusable="false" aria-hidden="true"><path d="M39.769,0C17.8,0,0,17.8,0,39.768c0,21.956,17.8,39.768,39.769,39.768   c21.965,0,39.768-17.812,39.768-39.768C79.536,17.8,61.733,0,39.769,0z M13.261,45.07V34.466h53.014V45.07H13.261z" fill-rule="evenodd" fill="#DE3618"></path></svg>');
/* * ****************************************
 *      SVG ICON CONSTANT BLOCK           *
 * **************************************** */






/* * ****************************************
 *  DATABASE TABLE NAME CONSTANT BLOCK    *
 * **************************************** */
define('TABLE_CLIENTS', 'clients');
define('TABLE_CLIENT_STORES', 'client_stores');

define('TABLE_SETTINGS', 'settings');
define('TABLE_FONT_FAMILY', 'font_family');

define('TABLE_SHIPPING_METHOD', 'shipping_methods');
/* * ****************************************
 * DATABASE TABLE NAME CONSTANT BLOCK END *
 * **************************************** */

/* Pagination decalre */
define('PAGE_PER', '5');

/* admin side error msg */

define("SOMETHING_WENT_WRONG_MSG", "Something went wrong");
define("SETTINGS_CREATED_SUCCESS_MSG", "Setting updated successfully");

define("PT_TEXT_COLOR_REQUIRED_MSG", "Product title text color required");
define("PT_TEXT_COLOR_FORMAT_MSG", "Product title text color should have hex value");
define("PT_FONT_SIZE_REQUIRED_MSG", "Product title font size required");
define("PT_FONT_SIZE_ONLYFLOAT_MSG", "Product title font size must be float value");

define("ORI_PRICE_COLOR_REQUIRED_MSG", "Original price color required");
define("ORI_PRICE_COLOR_FORMAT_MSG", "Original price color should have hex value");
define("ORI_PRICE_FONT_SIZE_REQUIRED_MSG", "Original price font size required");
define("ORI_PRICE_FONT_SIZE_ONLYFLOAT_MSG", "Original price font size must be float value");

define("DIS_PRICE_COLOR_REQUIRED_MSG", "Discount price color required");
define("DIS_PRICE_COLOR_FORMAT_MSG", "Discount price color should have hex value");
define("DIS_PRICE_FONT_SIZE_REQUIRED_MSG", "Discount price font size required");
define("DIS_PRICE_FONT_SIZE_ONLYFLOAT_MSG", "Discount price font size must be float value");

define("IMAGE_WIDTH_REQUIRED_MSG", "Image width required");
define("IMAGE_WIDTH_ONLYFLOAT_MSG", "Image width must be float value");

define("IMAGE_HEIGHT_REQUIRED_MSG", "Image height required");
define("IMAGE_HEIGHT_ONLYFLOAT_MSG", "Image height must be float value");

define("NOA_MESSAGE_REQUIRED_MSG", "None of above message required");

define("NOA_COLOR_REQUIRED_MSG", "None of above message color required");
define("NOA_COLOR_FORMAT_MSG", "None of above message color should have hex value");
define("NOA_FONT_SIZE_REQUIRED_MSG", "None of above message font size required");
define("NOA_FONT_SIZE_ONLYFLOAT_MSG", "None of above message font size must be float value");

define("POST_NOT_SUBMITED_ERR_MSG", "Post values are not submitted");

/* msg on operation (action like add,update,delete) */
define("SHIPPING_METHOD_UPDATED_SUCCESS_MSG", 'Shipping method updated successfully.');
define("SHIPPING_METHOD_CREATED_SUCCESS_MSG", 'Shipping method created successfully.');
define("SHIPPING_METHOD_RECORD_FETCHED_SUCCESS_MSG", 'Shipping method data fetched successfully.');
define("SHIPPING_METHOD_PLAN_EXPIRE_MSG", 'Opps ! Your plan is expired :).');
define("SHIPPING_METHOD_PLAN_NOT_SUBS_MSG", 'Opps ! You have not subcription :).');
define("SHIPPING_METHOD_STATUS_CHANGE_SUCCESS_MSG", 'Status changed successfully !');
define("SHIPPING_METHOD_DELETED_SUCCESS_MSG", 'Deleted successfully !');
define("SHIPPING_APP_STATUS_CHANGE_SUCCESS_MSG", 'App status changed successfully !');

/* End of all validation msg */
