<!--<div class="Polaris-Banner Polaris-Banner--withinPage Polaris-Banner--statusSuccess footer-review-bar" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner10Content" aria-labelledby="Banner10Heading">
    <div class="Polaris-Banner__Heading">
        <p class="Polaris-Heading"><span>&#10084;</span> We Value Your Feedback</p>
        <p>Please leave us a quick review in the App Store. Each review helps us tremendously!<br>We would love to hear your feedback and learn how we can improve the app, <a href="mailto:<?php echo SITE_EMAIL; ?>" target="_top">please let us a know.</a></p>
    </div>
    <div class="Polaris-Banner__Content" id="Banner10Content">
        <div class="Polaris-Banner__Actions">
            <div class="Polaris-ButtonGroup">
                <div class="Polaris-ButtonGroup__Item">
                    <a href="<?php echo APP_STORE_URL; ?>#modal-show=ReviewListingModal" target="_blank" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span>Rate our app</span></span></a>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="Polaris-Layout__Section">
    <div class="Polaris-FooterHelp">
        <div class="Polaris-FooterHelp__Content">
            <div class="Polaris-FooterHelp__Text">
                <?php echo SITE_COPYRIGHT; ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php include_once('footer_slider.php');?>
</div>
</main>
<!--Flash notice HTML -->
<div class="inline-flash-wrapper animated bounceInUp"> <div class="inline-flash"> <p class="inline-flash__message"></p> </div> </div>
<!--Flash notice HTML end -->
</body>
</html>
