<div class="Polaris-Page Polaris-Page--fullWidth">
    <div class="Polaris-Page__Content all-other-app">
        <div class="Polaris-Card">
            <div class="Polaris-Card__Header">
                <div class="Polaris-Page__Navigation">
                    <h2 class="Polaris-Heading">Our Other Popular Apps:</h2>
                    <div class="Polaris-Page__Pagination">
                        <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                            <button class="Polaris-Pagination__Button prev"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path></svg></span></button>
                            <button class="Polaris-Pagination__Button next"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16" fill-rule="evenodd"></path></svg></span></button>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="Polaris-Card__Section">
                <div id="footerSilder" class="owl-carousel">
                    <div class="Polaris-Card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">  
                                    <img src="https://www.appsonrent.com/appstation/assets/img/under-construction.png" alt="Under Construction" class="img-fluid">
                                </div>
                                <div class="col-sm-6">
                                    <div class="Polaris-TextContainer Polaris-TextContainer--spacingTight">
                                        <h2 class="Polaris-Heading">Under Construction</h2>
                                        <p>Capture leads and make brand name viral before site goes live</p>
                                        <a href="https://apps.shopify.com/under-construction" target="_top" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Polaris-Card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">  
                                    <img src="https://www.appsonrent.com/appstation/assets/img/on-time-delivery.png" alt="On Time Delivery" class="img-fluid">
                                </div>
                                <div class="col-sm-6">
                                    <div class="Polaris-TextContainer Polaris-TextContainer--spacingTight">
                                        <h2 class="Polaris-Heading">On Time Delivery</h2>
                                        <p>Let customers choose their delivery time and date</p>
                                        <a href="https://appsonrent.com/apps/on-time-delivery/" target="_top" class="Polaris-Button Polaris-Button--primary  Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Polaris-Card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">  
                                    <img src="https://www.appsonrent.com/appstation/assets/img/toppromobar.png" alt="Top Promo Bar" class="img-fluid">
                                </div>
                                <div class="col-sm-6">
                                    <div class="Polaris-TextContainer Polaris-TextContainer--spacingTight">
                                        <h2 class="Polaris-Heading">Top Promo Bar</h2>
                                        <p>Simply the easiest way to add a topbar to your website</p>
                                        <a href="https://apps.shopify.com/top-promo-bar" target="_top" class="Polaris-Button Polaris-Button--primary  Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Polaris-Card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">  
                                    <img src="https://www.appsonrent.com/appstation/assets/img/price-notification.png" alt="Product Notify by Appsonrent" class="img-fluid">
                                </div>
                                <div class="col-sm-6">
                                    <div class="Polaris-TextContainer Polaris-TextContainer--spacingTight">
                                        <h2 class="Polaris-Heading">Product Notify by Appsonrent</h2>
                                        <p>Make Sales With Automated Price update Notification</p>
                                        <a href="https://apps.shopify.com/price-notification" target="_top" class="Polaris-Button Polaris-Button--primary  Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Polaris-Card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">  
                                    <img src="https://www.appsonrent.com/appstation/assets/img/news-ticker.png" alt="News Ticker" class="img-fluid">
                                </div>
                                <div class="col-sm-6">
                                    <div class="Polaris-TextContainer Polaris-TextContainer--spacingTight">
                                        <h2 class="Polaris-Heading">News Ticker</h2>
                                        <p>Best way to build a news widget without coding anything</p>
                                        <a href="https://appsonrent.com/apps/news-ticker/" target="_top" class="Polaris-Button Polaris-Button--primary  Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Polaris-Card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">  
                                    <img src="https://www.appsonrent.com/appstation/assets/img/social-feed-gallery.png" alt="Ultimate Social Feed View" class="img-fluid">
                                </div>
                                <div class="col-sm-6">
                                    <div class="Polaris-TextContainer Polaris-TextContainer--spacingTight">
                                        <h2 class="Polaris-Heading">Ultimate Social Feed View</h2>
                                        <p>Increase engagement between you and your users</p>
                                        <a href="https://appsonrent.com/apps/social-feed-gallery/" target="_top" class="Polaris-Button Polaris-Button--primary  Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Polaris-Card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">  
                                    <img src="https://www.appsonrent.com/appstation/assets/img/tablepress.png" alt="TablePress" class="img-fluid">
                                </div>
                                <div class="col-sm-6">
                                    <div class="Polaris-TextContainer Polaris-TextContainer--spacingTight">
                                        <h2 class="Polaris-Heading">TablePress</h2>
                                        <p>Increase engagement between you and your users</p>
                                        <a href="https://appsonrent.com/apps/tablepress/" target="_top" class="Polaris-Button Polaris-Button--primary  Polaris-Button--sizeSlim"><span class="Polaris-Button__Content"><span>View App</span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>
        </div>
    </div>
</div>
